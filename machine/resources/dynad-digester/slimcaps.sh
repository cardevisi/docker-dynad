echo "INICIO - slimcaps"
date
START=$(date +%s)

### PARAMETROS ###
XML_URL=https://www.wine.com.br/wineinfo/lengow/xml/google-shopping.xml
XML_FILE=/mnt/TMP/xml/slimcaps.xml
TEMP_NAME=slimcaps
SCHEMA=dynad_slimcaps
DB_HOST=d3-asap-cdn6
EXP_HOST="d3-asap-cdn5 d3-asap-cdn6"
DIGESTER=/root/dynad_scripts/DigesterSlimcaps.groovy
SNFILE=/mnt/TMP/sna-files/slimcaps.txt
##################

RANKING_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old.4
mv $XML_FILE.old.3 $XML_FILE.old.4
mv $XML_FILE.old.2 $XML_FILE.old.3
mv $XML_FILE.old.1 $XML_FILE.old.2
mv $XML_FILE.old $XML_FILE.old.1

if [ -e "/mnt/TMP/sna-files/slimcaps_uolcliques.txt" ]; then
  rm /mnt/TMP/sna-files/slimcaps_uolcliques.txt
  echo "removed /mnt/TMP/sna-files/slimcaps_uolcliques.txt"
fi

if [ -e "/mnt/TMP/sna-files/slimcaps_uolcliques.zip" ]; then
  rm /mnt/TMP/sna-files/slimcaps_uolcliques.zip
  echo "removed /mnt/TMP/sna-files/slimcaps_uolcliques.zip"
fi

if [ -e "/mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.zip" ]; then
  rm /mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.zip
  echo "removed /mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.zip"
fi

if [ -e "/mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.txt" ]; then
  rm /mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.txt
  echo "removed /mnt/TMP/sna-files/slimcaps_uolcliques_topsellers.txt"
fi

mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."
wget $XML_URL -O $XML_FILE --http-user=ParceiroUOL --http-passwd=UOL123 

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]; then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi

cmp -s $XML_FILE $XML_FILE.old
cmp1=$?
echo "cmp1: $cmp1"

if [ $cmp1 -ne 0 ]; then
  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
    echo "exportando para $ip"
    mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done  

  if [ -s "/mnt/TMP/sna-files/slimcaps.txt" ]; then
    sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 37 1296000 slimcaps true || exit 1
  else
    echo "no data to work with - SNA"
  fi

else
  if [ $RANKING -eq 1 ]; then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
      echo "exportando para $ip"
      mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done      
   
    if [ -s "/mnt/TMP/sna-files/slimcaps.txt" ]; then
      sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 37 1296000 slimcaps true || exit 1
    else
      echo "no data to work with - SNA"
    fi
  else
    echo "*** nada a fazer ..."
  fi
fi

date
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "operacao finalizada em $DIFF segundos - $SCHEMA"
echo "FIM***"