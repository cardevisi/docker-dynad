echo "INICIO - Zona Sul Vinhos"
date
START=$(date +%s)

### PARAMETROS ###
XML_URL=https://www.wine.com.br/wineinfo/lengow/xml/google-shopping.xml
XML_FILE=/mnt/TMP/xml/wine.xml
TEMP_NAME=wine
SCHEMA=dynad_wine
DB_HOST=d3-asap-cdn6
EXP_HOST="d3-asap-cdn5 d3-asap-cdn6"
DIGESTER=/root/dynad_scripts/DigesterWine.groovy
SNFILE=/mnt/TMP/sna-files/wine.txt
##################


RANKING_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old.4
mv $XML_FILE.old.3 $XML_FILE.old.4
mv $XML_FILE.old.2 $XML_FILE.old.3
mv $XML_FILE.old.1 $XML_FILE.old.2
mv $XML_FILE.old $XML_FILE.old.1

mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."
wget $XML_URL -O $XML_FILE

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi

cmp -s $XML_FILE $XML_FILE.old >/dev/null 
if [ $? -ne 0 ]; then
  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
    echo "exportando para $ip"
    mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done  
  if [ -s "/mnt/TMP/sna-files/wine.txt" ]; then
    sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 40 1296000 wine true || exit 1
  else
    echo "no data to work with - SNA"
  fi

else
  if [ $RANKING -eq 1 ]; then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
      echo "exportando para $ip"
      mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done    

    if [ -s "/mnt/TMP/sna-files/wine.txt" ]; then
      sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 40 1296000 wine true || exit 1
    else
      echo "no data to work with - SNA"
    fi
  else
    echo "*** nada a fazer ..."
  fi
fi

date
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "operacao finalizada em $DIFF segundos - $SCHEMA"
echo "FIM***"