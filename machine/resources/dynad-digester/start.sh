#!bin/bash

echo "::: INICIO DA AUTOMACAO :::"

#mkdir c:/casi/
#cd c:/casi/
#touch aqui.txt

#XML_URL=http://www.google.com.br #http://xmlparceiros.flamengoloja.com.br/asapcode/catalog.xml
#XML_FILE=text.xml #/mnt/XML/centauro_flamengo.xml

#wget $XML_URL -O $XML_FILE

alias sqls="cd ./sqls/"

echo "::: RUNNING MYSQL START:::"

if [ -d /etc/init.d/mysql ]; then
    /etc/init.d/mysql start;
else 
    echo "::: DIRETORIO NÃO EXISTE = /etc/init.d/mysql :::"
fi

echo "::: CHANGE TO FOLDER SQLS :::"

sqls

if [ -d /var/lib/mysql/$1 ]; then
    mysql -u root -p123 -e "drop database dynad_$1; exit;"
fi

echo "::: CREATE DATABASE AND USE :::"

#mysql -u root -p123 -e "create database dynad_$1; exit;"

#mysql -u root -p123 -e "use dynad_$1; source estrutura.sql; source normaliza_moeda.sql; exit;"

echo "::: END :::"

/bin/bash