echo "INICIO - Linio MX"

date
START=$(date +%s)

### PARAMETROS ###
XML_URL=http://www.linio.com.mx/dynad.xml
XML_FILE=/mnt/XML/liniomx.xml
TEMP_NAME=linmx
SCHEMA=dynad_linio
DB_HOST=74.81.70.46
EXP_HOST="64.31.10.122 74.81.70.46 "
DIGESTER=/root/dynad_scripts/DigesterLinio.groovy
##################





RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old
mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."
wget $XML_URL -O $XML_FILE
#curl $XML_URL |  iconv -f iso8859-1 -t utf-8 > $XML_FILE
perl -pe "s/encoding=\"ISO-8859-1\"/ /g" $XML_FILE > $XML_FILE.tmp 
mv $XML_FILE.tmp $XML_FILE
enconv -L none -x ISO-8859-1 $XML_FILE 2>/dev/null



echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi




cmp -s $XML_FILE $XML_FILE.old >/dev/null 
if [ $? -ne 0 ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
     echo "exportando para $ip"
     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done


else

  if [ $RANKING -eq 1 ]
  then
    echo "executando script de importacao ..."  
#    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
       echo "exportando para $ip"
       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done


  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"

