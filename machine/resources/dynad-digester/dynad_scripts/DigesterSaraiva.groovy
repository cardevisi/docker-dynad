import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class Spider {
	static int key;
	static {
		ConnHelper.schema = "dynad_saraiva";
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
		ResultSet res = pStmt.executeQuery();
		if (res.next()) key = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
	}

	public static boolean _DEBUG_ = false;

	private static markXml(String file) {
		Connection conn = ConnHelper.get().reopen();
		int cline = 0;
		def p = null;
		String line = null;

		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, codigo, marca, nome, descricao,promover, desconto;
		Long score, classe

		def summary = new XmlParser().parse(file);
		def list = summary.product

		list.each {
			/*<product id="100016">
			    <name> <![CDATA[ N 1 Only Hits ]]> </name>
			    <smallimage>http://images.livrariasaraiva.com.br/imagemnet/imagem.aspx?pro_id=100016</smallimage>
			    <bigimage>http://images.livrariasaraiva.com.br/imagemnet/imagem.aspx?pro_id=100016</bigimage>
			    <producturl><![CDATA[http://www.saraiva.com.br/produto/100016/?utm_source=criteo&utm_medium=me-rtg&utm_campaign=xml-todosformatos_100016]]>
			    </producturl>
			    <description><![CDATA[Coletânea com sucessos de artistas como Aerosmith, REM, DestinyS Child, New Order, Creed e outros.]]></description>
			    <retailprice>30.90</retailprice>
			    <price>29.36</price>
			    <discount/>
			    <recommendable>1</recommendable>
			    <instock>1</instock>
			    <CategoryID1><![CDATA[ Música ]]>-<![CDATA[ CD ]]></CategoryID1>
			</product>*/
			sku = it['@id']
			nome = it.name.text()
			url = it.producturl.text()
			if(url.indexOf("?") > -1) {
				def c = -1
				if((c = url.indexOf('&utm_source')) > -1) url = url.substring(0, c);
				if((c = url.indexOf('&utm_campaign')) > -1) url = url.substring(0, c);
				if((c = url.indexOf('&utm_medium')) > -1) url = url.substring(0, c);
			}
			fprice = Digester.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR"));
			oprice = Digester.autoNormalizaMoeda(it.retailprice.text(), false, new Locale("pt", "BR"));
			nparcelas = '1';
			vparcelas = fprice;
			image = it.bigimage.text()
			score = 0
			c1 = it.CategoryID1.text()
			c2 = c1
			c3 = c1
			marca = ''
			descricao = it.description.text()
			if(descricao.length()>512)
				descricao = descricao.substring(0, 512);
			fprice = "R\$ " + fprice == null || fprice == "" ? "0,00" : fprice;
			oprice = "R\$ " + oprice == null || oprice == "" ? "0,00" : oprice;
			vparcelas = "R\$ " + vparcelas == null || vparcelas == "" ? "0,00" : vparcelas;


			FieldMetadata fSku = new FieldMetadata(columnName: 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
            FieldMetadata fAtivo = new FieldMetadata(columnName: 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
            java.util.List < FieldMetadata > fields = new java.util.ArrayList < FieldMetadata > ();
            fields.add(new FieldMetadata(columnName: 'categoria', columnType: 'string', columnValue: c1 + '/' + c2, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG'));
            fields.add(new FieldMetadata(columnName: 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK'));
            fields.add(new FieldMetadata(columnName: 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA'));

            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
            if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
		}

	}

	public static void main(String[] args) {
		if (args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml('/mnt/XML/saraiva.xml');
			//Digester.normalizaSupernova(ConnHelper.get().reopen());
			//Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			//Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.NONE);
			//Digester.bestSellers(ConnHelper.get().reopen());
			//Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
			//Digester.validacao(ConnHelper.get().reopen());
		} else if (args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
		}
	}
}

