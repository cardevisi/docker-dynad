export GH=/usr
export JH=/var/java
export SD=/root/dynad_scripts

echo "Acessando diretorio de scripts"
cd $SD/patch_dell

echo "Removendo historico"
rm XlsXToXML.class

echo "Compilando classe"
$JH/bin/javac -cp poi-3.10-FINAL-20140208.jar:ooxml-schemas-1.1.jar:poi-ooxml-3.10-FINAL.jar XlsXToXML.java

echo "Removendo XML"
rm /mnt/XML/dell.xml

echo "Convertendo arquivo"
$JH/bin/java -cp .:poi-3.10-FINAL-20140208.jar:ooxml-schemas-1.1.jar:poi-ooxml-3.10-FINAL.jar:XlsXToXML.class:xmlbeans-2.4.0.jar:dom4j-20040902.021138.jar XlsXToXML xml_uol.xlsx /mnt/XML/dell.xml

#cd $SD
#echo "Processando XML"
#$GH/bin/groovy DigesterDell.groovy
