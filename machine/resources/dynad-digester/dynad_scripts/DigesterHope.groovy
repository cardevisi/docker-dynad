import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_hope";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                def list = summary.PRODUTO

                list.each {
/*


  <PRODUTO>
    <ID_PRODUTO>000I0030</ID_PRODUTO>
    <LINK_PRODUTO>http://www.hopelingerie.com.br/prod/5225/Cobertura-adesiva-de-mamilos.aspx</LINK_PRODUTO>
    <DESCRICAO><![CDATA[Cobertura adesiva de mamilos]]></DESCRICAO>
    <PRECO>20,00</PRECO>
    <PARCELAMENTO>ou 1x de R$20,00</PARCELAMENTO>
    <DISPONIBILIDADE>Disponível</DISPONIBILIDADE>
    <IMAGEM>http://www.hopelingerie.com.br/Imagens/produtos/NU/000I0030_NU/000I0030_NU_Vitrine.gif</IMAGEM>
    <CATEGORIA>ACESSÓRIOS</CATEGORIA>
  </PRODUTO>
  <PRODUTO>


*/
                                sku = it.ID_PRODUTO.text()
                                nome = Digester.stripTags(Digester.ISO2UTF8(it.DESCRICAO.text()))
                                //nome = it.nome_produto.text()
                                url = it.LINK_PRODUTO.text()

				if(url.indexOf("?") > -1)
					url = url.substring(0, url.indexOf("?"));

                                fprice = Digester.autoNormalizaMoeda(it.PRECO.text(), false, new Locale("pt", "BR"));
                                oprice = Digester.autoNormalizaMoeda(it.PRECO.text(), false, new Locale("pt", "BR"));
				java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");

                                def tmp = it.PARCELAMENTO.text()

                                if(tmp.indexOf("x") > -1) { 
					tmp = tmp.substring(0, tmp.indexOf("x"));
                                	nparcelas = tmp.replace("ou", "").trim();
                                	tmp = it.PARCELAMENTO.text();
					tmp = tmp.substring(tmp.indexOf("x")+1);
                                	vparcelas = Digester.autoNormalizaMoeda(tmp.replace("R\$","").trim(), true, new Locale("pt", "BR"));
				}

                        	image = it.IMAGEM.text()

				tmp = it.CATEGORIA.text();
				def categs = tmp.split(":");

                         	c1 = categs.length>2?categs[categs.length-3].trim():(categs.length>=1?categs[0].trim():tmp.trim())
                         	c2 = categs.length>2?categs[categs.length-2].trim():(categs.length==2?categs[1].trim():"")
                         	c3 = categs.length>2?categs[categs.length-1].trim():""

                         	marca = it.Fabricante.text()

				def pt = image.toLowerCase().indexOf('.png')>-1?image.toLowerCase().indexOf('.png') : ( 
					 image.toLowerCase().indexOf('.gif')>-1?image.toLowerCase().indexOf('.gif') : (
					 image.toLowerCase().indexOf('.jpg')>-1?image.toLowerCase().indexOf('.jpg') : -1 
				) );

				if(pt > -1) image = image.substring(0,pt+4); 
	
				fprice = "R\$ " + fprice==null||fprice==""?"0,00":fprice;
				oprice = "R\$ " + oprice==null||oprice==""?"0,00":oprice;
				vparcelas = "R\$ " + vparcelas==null||vparcelas==""?"0,00":vparcelas;
				
//println("sku:"+sku+";nome:"+nome+";url:"+url+";fprice:"+fprice+";oprice:"+oprice+";nparcelas:"+nparcelas+";vparcelas:"+vparcelas + ";" + c1 + "/" + c2 + "/" + c3);
	                                Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false);
 
				
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/hope.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
			//Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

        }
	
}
