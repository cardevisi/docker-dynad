import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import java.net.*;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterV2 extends Digester {

	public static void salvaCatalogo (Connection conn, String sku, String c1, String c2, String c3,
				String imagem, String precoOriginal, String precoPromocional, String numeroParcelas, String valorParcelas,
				String link, String marca, String nome, String codigo, String ativo, Boolean encodeImage, String gratis, String c4, String c5) {


            def cnt;
            String recomendacoes;
            PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where sku = ?");
            pStmt.setString(1, sku);
            ResultSet res = pStmt.executeQuery();

			def categoria = '';
			categoria += c1 != null && c1 != '' ? c1 : '';
			categoria += c2 != null && c2 != '' ? '_'+c2 : '';
			categoria += c3 != null && c3 != '' ? '_'+c3 : '';
			if( c4 != null ) categoria += '_' + c4;
			if( c5 != null ) categoria += '_' + c5;

			ativo = (ativo=='sim'||ativo=='1')?'1':'0';

	        if( res.next() )
            	cnt = res.getInt(1);
            ConnHelper.closeResources(pStmt, res);

			if(encodeImage && imagem != null && imagem != '')
	            //imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=o8vtEg01qmUcZL-H_Dgv6Q"; //s=200x200
	            imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=ElXEFX1LcBgiF7zWSd1M-A"; //f=180x180&cr=3

            int col = 1;
            String stmt = "";

			def selo = 'novidade';
			def desconto = '';
			gratis = null;

            if( cnt == 0) {
				if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
					selo = 'desconto';
					desconto = Math.floor( ( 1 - ( Double.parseDouble(DigesterV2.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(DigesterV2.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
				}

				stmt = """insert into catalogo ( 
					id, categoria, categoria1, categoria2, categoria3, """ + (c4 != null ? " categoria4, " : "") + """ """ + (c5 != null ? " categoria5, " : "") + """ nome, imagem, link, preco_original, preco_promocional,
					numero_parcelas, valor_parcelas, marca, status, codigo, recomendacoes, ativo, versao, selo, desconto, """ +(gratis!=null?"gratis, ":"")+ """sku, last_update, created_in) 
					select ifnull(max(id), 0) + 1, ?, ?, ?, ? """ + (c4 != null ? " ,? " : "") + """ """ + (c5 != null ? " , ? " : "") + """ , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now() from catalogo """;

            } else {
				desconto = '';
				if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
					selo = 'desconto';
					desconto = Math.floor( ( 1 - ( Double.parseDouble(DigesterV2.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(DigesterV2.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
				}

				stmt = """update catalogo set
						categoria = ?,
		                categoria1 = ?,
		                categoria2 = ?,
		                categoria3 = ?,
						""" + (c4 != null ? " categoria4 = ?, " : "") + """
						""" + (c5 != null ? " categoria5 = ?, " : "") + """
		                nome = ?,
		                imagem = ?,
		                link = ?,
		                preco_original = ?,
		                preco_promocional = ?,
		                numero_parcelas = ?,
		                valor_parcelas = ?,
		                marca = ?,
		                status = ?,
						codigo = ?,
		                recomendacoes = ?,
		                ativo = if(ativo <> ?, if(ativo = '1', '9', '1'), ativo),
		                versao = ?,
						selo = ?,
						desconto = ?,
						""" +(gratis!=null?"gratis = ?, ":"")+ """
						last_update = now()
			        where sku = ? AND (
                        nome <> ? OR
                        imagem <> ? OR
                        link <> ? OR
                        preco_original <> ? OR
                        preco_promocional <> ? OR
                        valor_parcelas <> ? OR
                        categoria <> ? OR
                        ativo <> ? )""";
            }


            pStmt = conn.prepareStatement(stmt.replaceAll("\\n", " "));
			def idx = 1;

            try {
				pStmt.setString(idx++, categoria) ;
				pStmt.setString(idx++, c1) ;
				pStmt.setString(idx++, c2) ;
				pStmt.setString(idx++, c3) ;
				if( c4 != null )
					pStmt.setString(idx++, c4) ;
				if( c5 != null )
					pStmt.setString(idx++, c5) ;

				pStmt.setString(idx++, nome) ;
				pStmt.setString(idx++, imagem) ;
				pStmt.setString(idx++, link) ;
				pStmt.setString(idx++, precoOriginal) ;
				pStmt.setString(idx++, precoPromocional) ;
				pStmt.setString(idx++, numeroParcelas) ;
				pStmt.setString(idx++, valorParcelas) ;
				pStmt.setString(idx++, marca) ;
				pStmt.setString(idx++, '1') ;
				pStmt.setString(idx++, codigo) ;
				pStmt.setString(idx++, recomendacoes) ;
				pStmt.setString(idx++, ativo) ;
				pStmt.setLong(idx++, (long)System.currentTimeMillis()/1000) ;
				pStmt.setString(idx++, selo) ;
				pStmt.setString(idx++, "" + desconto) ;
				if(gratis != null)
					pStmt.setString(idx++, "" + gratis) ;
				pStmt.setString(idx++, sku) ;


				if(stmt.toLowerCase().trim().startsWith("update") ) {
					pStmt.setString(idx++, nome) ;
					pStmt.setString(idx++, imagem) ;
					pStmt.setString(idx++, link) ;
					pStmt.setString(idx++, precoOriginal) ;
					pStmt.setString(idx++, precoPromocional) ;
					pStmt.setString(idx++, valorParcelas) ;
					pStmt.setString(idx++, categoria) ;
					pStmt.setString(idx++, ativo) ;
				}

				def ret = pStmt.executeUpdate();
				ConnHelper.closeResources(pStmt, null);

				if(stmt.toLowerCase().trim().startsWith("insert") )
					println(sku + " -> ..N.O.V.O... -> " + nome);
				else if(ret > 0)
					println(sku + " -> .ATUALIZADO. -> " + nome);
				else {
					println(sku + " -> ....none.... -> " + nome);
					stmt = "update catalogo set last_update = now(), selo = if(preco_original <> preco_promocional, 'desconto', if(DATEDIFF(now(), created_in) <= " + _NOVIDADE_PERIODO_ + ", 'novidade', '')) where sku = ?";
					pStmt = conn.prepareStatement(stmt);
					pStmt.setString(1, sku) ;
					pStmt.executeUpdate();
					ConnHelper.closeResources(pStmt, null);
				}


            } catch(ex) {
	            println("erro: [" + sku + "]");
	            ex.printStackTrace();
            }

        }


	public static void salvaCatalogoMetadata (Connection conn, FieldMetadata sku, FieldMetadata ativo, java.util.List<FieldMetadata> fields ) {
            int cnt;
            String recomendacoes;
			
			ativo.columnValue = (ativo.columnValue=='sim'||ativo.columnValue=='1')?'1':'0';

            PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where "+ sku.columnName +" = ?");
            sku.setStmtParameter( 1, pStmt );
            ResultSet res = pStmt.executeQuery();
	        if( res.next() )
            	cnt = res.getInt(1);
            ConnHelper.closeResources(pStmt, res);

            String stmt = "";
			pStmt = null;
			String dbg="";
			if( cnt == 0) {
				stmt += "INSERT INTO catalogo (id, last_update, created_in, sku, ativo";
				for( FieldMetadata f : fields ) {
					stmt += ',' + f.columnName;
				}

				stmt += ') SELECT ifnull(max(id), 0) + 1, now(), now(), ?, ?';
				for( FieldMetadata f : fields ) 
					stmt += ",?";
				stmt += " FROM catalogo";
				pStmt = conn.prepareStatement(stmt);
				sku.setStmtParameter( 1, pStmt );
				ativo.setStmtParameter( 2, pStmt );
				int currentFieldIndex = 3;
				for( FieldMetadata f : fields )
					f.setStmtParameter( currentFieldIndex++, pStmt );
			} else { 
				stmt += "UPDATE catalogo SET ativo = if(ativo <> ?, if(ativo = '1', '9', '1'), ativo), last_update = now() ";
				for( FieldMetadata f : fields )
					stmt += ',' + f.columnName + ' = ? ';

				stmt += " WHERE sku = '"+sku.columnValue+"' AND ( ativo <> '"+ativo.columnValue+"' ";	
				dbg += " WHERE sku = '"+sku.columnValue+"' AND ( ativo <> '"+ativo.columnValue+"' ";	
				for( FieldMetadata f : fields ) { 
					if( f.lookupChange )
						stmt += ' OR (' + f.columnName + ' <> ? OR ( ' + f.columnName + ' is null and isnull(?) = false ) ) ';
				}
				stmt += ' )';

				pStmt = conn.prepareStatement(stmt);
				ativo.setStmtParameter(1, pStmt );
				
				int currentFieldIndex = 2;
				for( FieldMetadata f : fields )
					f.setStmtParameter( currentFieldIndex++, pStmt );

				//sku.setStmtParameter( currentFieldIndex++, pStmt );
				//ativo.setStmtParameter( currentFieldIndex++, pStmt );
				//ativo.setStmtParameter( currentFieldIndex++, pStmt );
				for( FieldMetadata f : fields ) {
					if( f.lookupChange ) {
						f.setStmtParameter( currentFieldIndex++, pStmt );
						f.setStmtParameter( currentFieldIndex++, pStmt );
						dbg += f.columnName + "=" + f.columnValue + "; ";
					}
				}
			}

			try {
				def ret = pStmt.executeUpdate();
				ConnHelper.closeResources(pStmt, null);

				if(stmt.toLowerCase().trim().startsWith("insert") )
					println(sku.columnValue + " -> ..N.O.V.O..." );
				else if(ret > 0) {
				//	println( dbg );
					println(sku.columnValue + " -> .ATUALIZADO." );
				} else {
					println(sku.columnValue + " -> ....none...." );
					stmt = "update catalogo set last_update = now(), selo = if(preco_original <> preco_promocional, 'desconto', if(DATEDIFF(now(), created_in) <= " + _NOVIDADE_PERIODO_ + ", 'novidade', '')) where sku = ?";
					pStmt = conn.prepareStatement(stmt);
					sku.setStmtParameter( 1, pStmt );
					pStmt.executeUpdate();
					ConnHelper.closeResources(pStmt, null);
				}
            } catch(ex) {
	            println("erro: [" + sku.columnValue + "]");
	            ex.printStackTrace();
            }

        }

	private static void completaRecomendacoesDafiti(Connection conn) { 
		print("completando recomendacoes Dafiti... ");
		def qry = null


		qry = """select sku, recomendacoes, recomendacao1, recomendacao2, recomendacao3, recomendacao4, recomendacao5, 
				recomendacao6, recomendacao7, recomendacao8, recomendacao9, recomendacao10 from catalogo""";
				
		PreparedStatement pStmt = conn.prepareStatement(qry);
		ResultSet res = pStmt.executeQuery();
		
		def recs = [:]
		while( res.next() ) {
			def recomendacoes = "";
			if(res.getString(3)!="" && res.getString(3)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(3);
			if(res.getString(4)!="" && res.getString(4)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(4);
			if(res.getString(5)!="" && res.getString(5)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(5);
			if(res.getString(6)!="" && res.getString(6)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(6);
			if(res.getString(7)!="" && res.getString(7)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(7);
			if(res.getString(8)!="" && res.getString(8)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(8);
			if(res.getString(9)!="" && res.getString(9)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(9);
			if(res.getString(10)!="" && res.getString(10)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(10);
			if(res.getString(11)!="" && res.getString(11)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(11);
			if(res.getString(12)!="" && res.getString(12)!=null) recomendacoes += (recomendacoes==""?"":",") + res.getString(12);
			def cnt = recomendacoes.split(",").length;
			if(cnt < 20) {
				def recsnossas = res.getString(2).split(",")
				recsnossas.each {
					if(recomendacoes.indexOf(it)==-1) {
						if(it!="" && it!=null) recomendacoes += (recomendacoes==""?"":",") + it;
						if(++cnt == 19) return;
					}
				}
			}
			recs[res.getString(1)] = recomendacoes;
			//println("--->" + recomendacoes);
		}
		
		recs.each { key,value->
			qry = "update catalogo set recomendacoes_bi = '" + value + "' where sku = '" + key + "'";
			pStmt = conn.prepareStatement(qry);
			print(pStmt.executeUpdate()==1?'.':'');
		}
	}
					
					
	private static void completaRecomendacoes(Connection conn, Boolean reverso) { 
		this.completaRecomendacoes(conn, reverso, 11);
	}

	private static void completaRecomendacoes(Connection conn, Boolean reverso, Integer tamanho) { 
		this.completaRecomendacoes(conn, reverso, tamanho, RecommenderMode.SAME_PRICE);
	}
	
	
	private static void completaRecomendacoes(Connection conn, Boolean reverso, Integer tamanho, RecommenderMode recommenderMode) {
		this.completaRecomendacoes(conn, reverso, tamanho, recommenderMode, 'recomendacoes');
	}
	
	private static void completaRecomendacoes(Connection conn, Boolean reverso, Integer tamanho, RecommenderMode recommenderMode, String recommenderField) { 
		print("completando recomendacoes ... ");
		print("criando tab temoraria ... ");
		PreparedStatement pTmp = null;
		pTmp = conn.prepareStatement("""drop table if exists catalogo_tmp""");
        pTmp.executeUpdate();
		
		def qry = null


		if(recommenderMode == RecommenderMode.GEO_LOCATION || recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY) {
			qry = """
				create table catalogo_tmp(
					INDEX(id), 
					INDEX(desconto), 
					INDEX(sku), 
					INDEX(score), 
					INDEX(categoria1), 
					INDEX(categoria2), 
					INDEX(categoria3), 
					INDEX(preco_promocional),
					INDEX(latitude),
					INDEX(longitude),
					INDEX(pais),
					INDEX(estado),
					INDEX(cidade),
					INDEX(link) ) engine=memory 
				select id, desconto, sku, score, 
					IFNULL(categoria1, 'empty') as categoria1, 
					IFNULL(categoria2, 'empty') as categoria2, 
					IFNULL(categoria3, 'empty') as categoria3, 
					IFNULL(categoria4, 'empty') as categoria4, 
					IFNULL(categoria5, 'empty') as categoria5, 
					autoNormalizaMoeda(preco_promocional) as preco_promocional, latitude, longitude, pais, estado, cidade, link from catalogo where ativo = '1'
				ORDER BY """ +
				( reverso 
					? """ categoria5, categoria4, categoria3, categoria2, categoria1 """
					: """ categoria1, categoria2, categoria3, categoria4, categoria5 """
				);
		} else if(recommenderMode == RecommenderMode.DIFFERENT_NAME || recommenderMode == RecommenderMode.DIFFERENT_NAMELINK) {
			qry = """
				create table catalogo_tmp(
					INDEX(id), 
					INDEX(desconto), 
					INDEX(sku), 
					INDEX(score), 
					INDEX(categoria1), 
					INDEX(categoria2), 
					INDEX(categoria3), 
					INDEX(preco_promocional),
					INDEX(nome),
					INDEX(link) ) engine=memory 
				select id, desconto, sku, score, 
					IFNULL(categoria1, 'empty') as categoria1, 
					IFNULL(categoria2, 'empty') as categoria2, 
					IFNULL(categoria3, 'empty') as categoria3, 
					IFNULL(categoria4, 'empty') as categoria4, 
					IFNULL(categoria5, 'empty') as categoria5, 
					autoNormalizaMoeda(preco_promocional) as preco_promocional, nome, link from catalogo where ativo = '1'
				ORDER BY """ +
				( reverso 
					? """ categoria5, categoria4, categoria3, categoria2, categoria1 """
					: """ categoria1, categoria2, categoria3, categoria4, categoria5 """
				);
		} else {
			qry = """
				create table catalogo_tmp(
					INDEX(id), 
					INDEX(desconto), 
					INDEX(sku), 
					INDEX(score), 
					INDEX(categoria1), 
					INDEX(categoria2), 
					INDEX(categoria3),
					INDEX(categoria4),
					INDEX(categoria5), 
					INDEX(preco_promocional) ) engine=memory 
				select id, sku, score, 
					IFNULL(categoria1, 'empty') as categoria1, 
					IFNULL(categoria2, 'empty') as categoria2, 
					IFNULL(categoria3, 'empty') as categoria3, 
					IFNULL(categoria4, 'empty') as categoria4, 
					IFNULL(categoria5, 'empty') as categoria5, 
					desconto, autoNormalizaMoeda(preco_promocional) as preco_promocional from catalogo where ativo = '1'
				ORDER BY """ +
				( reverso 
					? """ categoria5, categoria4, categoria3, categoria2, categoria1 """
					: """ categoria1, categoria2, categoria3, categoria4, categoria5 """
				);

		}

		pTmp = conn.prepareStatement(qry);
		pTmp.executeUpdate();
		ConnHelper.closeResources(pTmp, null);
		println("OK");

		if(recommenderMode == RecommenderMode.GEO_LOCATION || recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY) {
			qry = """
			SELECT 
				a.id_categoria,
				a.id,
				a.""" + recommenderField + """,
				LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) ,
				IFNULL(a.categoria1, 'empty'),
				IFNULL(a.categoria2, 'empty'),
				IFNULL(a.categoria3, 'empty'),
				autoNormalizaMoeda(a.preco_promocional),
				IFNULL(a.categoria4, 'empty'),
				IFNULL(a.categoria5, 'empty'),
				a.link,
				latitude, longitude, pais, estado, cidade
			FROM 
				catalogo a
			WHERE 
				( LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) < """ + (tamanho - 1) + """ )
				
			"""
		} else if(recommenderMode == RecommenderMode.DIFFERENT_NAME || recommenderMode == RecommenderMode.DIFFERENT_NAMELINK) {
			qry = """
			SELECT 
				a.id_categoria,
				a.id,
				a.""" + recommenderField + """,
				LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) ,
				IFNULL(a.categoria1, 'empty'),
				IFNULL(a.categoria2, 'empty'),
				IFNULL(a.categoria3, 'empty'),
				autoNormalizaMoeda(a.preco_promocional),
				IFNULL(a.categoria4, 'empty'),
				IFNULL(a.categoria5, 'empty'),
				a.link,
				nome
			FROM 
				catalogo a
			WHERE 
				( LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) < """ + (tamanho - 1) + """ )
				
			"""
		} else {
			qry = """
			SELECT 
				a.id_categoria,
				a.id,
				a.""" + recommenderField + """,
				LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) ,
				IFNULL(a.categoria1, 'empty'),
				IFNULL(a.categoria2, 'empty'),
				IFNULL(a.categoria3, 'empty'),
				autoNormalizaMoeda(a.preco_promocional),
				IFNULL(a.categoria4, 'empty'),
				IFNULL(a.categoria5, 'empty'),
				a.link
			FROM 
				catalogo a
			WHERE 
				( LENGTH(ifnull(a.""" + recommenderField + """, '')) - LENGTH(REPLACE(ifnull(a.""" + recommenderField + """, ''), ',', '')) < """ + (tamanho - 1) + """ )
				
			"""
		}


		def categoria1, categoria2, categoria3, categoria4, categoria5;
		PreparedStatement pStmt = conn.prepareStatement(qry);
		ResultSet res = pStmt.executeQuery();
		while( res.next() ) {
			categoria1 = res.getString(5);
			categoria2 = res.getString(6);
			categoria3 = res.getString(7);
			categoria4 = res.getString(9);
			categoria5 = res.getString(10);
			def link = res.getString(11);

			def recom = res.getString( 3 );
			if(recom == null) recom = "";
			recom = "'" + recom.replaceAll(",", "','") + "'";
			int total = res.getInt(4); total += (recom=="''") ? 0 : 1;
			def preco_referencia = res.getFloat(8);

			def latitude = null;
			def longitude = null;
			def pais = null;
			def estado = null;
			def cidade = null;
			def nome = null;

			if(recommenderMode == RecommenderMode.GEO_LOCATION || recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY) {
				latitude = res.getFloat(12);
				longitude = res.getFloat(13);
				pais = res.getString(14);
				estado = res.getString(15);
				cidade = res.getString(16);
            } else if(recommenderMode == RecommenderMode.DIFFERENT_NAME || recommenderMode == RecommenderMode.DIFFERENT_NAMELINK) {
				nome = res.getString(12);
				if(nome.length() > 10) nome = nome.substring(0, (int)Math.floor(nome.length()/2));
				else nome = nome.substring(0, nome.length() - 5);
				nome = nome.replaceAll("'", "");
			}

			qry = """
					SELECT 
						distinct a.sku
						""" + (recommenderMode == RecommenderMode.DIFFERENT_LINK ? ",a.link " : "") + """
					FROM 
						catalogo_tmp a
					WHERE 
						a.id <> ?
						AND a.sku not in (""" + recom + """) 
					ORDER BY """ +
						( recommenderMode == RecommenderMode.GEO_LOCATION || recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY ? 
							""" """ : 
							( 
								( reverso ? 
									( """ if( """ + 
										""" a.categoria5=? and a.categoria4=? and a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, """ + 
										""" if( a.categoria5=? and a.categoria4=? and a.categoria3=? and a.categoria2=?, 1, """ +
											""" if( a.categoria5=? and a.categoria4=? and a.categoria3=? , 2, """ +
												""" if( a.categoria5=? and a.categoria4=? , 3, """ +
													""" if( a.categoria5=?, 4, 5 )""" +
												""" ) """ +
											""" ) """ +
										""" ) """ + 
								 	 """  ) """ 
									)
									: 
									( """ if( """ + 
										""" a.categoria5=? and a.categoria4=? and a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, """ + 
										""" if( a.categoria1=? and a.categoria2=? and a.categoria3=? and a.categoria4=?, 1, """ +
											""" if( a.categoria1=? and a.categoria2=? and a.categoria3=? , 2, """ +
												""" if( a.categoria1=? and a.categoria2=? , 3, """ +
													""" if( a.categoria1=?, 4, 5 )""" +
												""" ) """ +
											""" ) """ +
										""" ) """ + 
								  	"""  ) """ 
								  	)
								) + ","
							).concat(' ') 
						).
						concat ( recommenderMode == RecommenderMode.SAME_PRICE ? 
							"abs(a.preco_promocional - ?)," : 
							( recommenderMode == RecommenderMode.BEST_PRICE || recommenderMode == RecommenderMode.DIFFERENT_LINK ? 
								"a.preco_promocional," : 
								( 
									( recommenderMode == RecommenderMode.DIFFERENT_PRICE ? 
										"abs(a.preco_promocional - ?) DESC," : 
										( recommenderMode == RecommenderMode.GEO_LOCATION ? 
											"if(a.pais=? and a.estado=? and a.cidade=?, 0, if(a.pais=? and a.estado=?, 1, if(a.pais=?, 2, 3))), abs(get_distance(latitude, longitude, " + latitude + "," + longitude + "))," : 
											( recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY ? 
												"if(a.pais=? and a.estado=? and a.cidade <> ?, 0, if(a.pais=? and a.estado=? and a.cidade = ?, 3, if(a.pais=? and a.estado=?, 1, if(a.pais=?, 2, 4) ) )), abs(get_distance(latitude, longitude, " + latitude + "," + longitude + "))," : 
												""
											) 
										)
									) 
								) 
							) 
						) + """
						a.score DESC
					LIMIT """ + (tamanho-total);

			PreparedStatement pStmt2 = conn.prepareStatement( qry );
			pStmt2.setString(1, res.getString(2));
			int indexParameter = 2;
			if( recommenderMode != RecommenderMode.GEO_LOCATION && recommenderMode != RecommenderMode.GEO_LOCATION_DIFFERENT_CITY ) { 
	            if(reverso) {
					pStmt2.setString(indexParameter++, categoria5);
					pStmt2.setString(indexParameter++, categoria4);
					pStmt2.setString(indexParameter++, categoria3);
					pStmt2.setString(indexParameter++, categoria2);
					pStmt2.setString(indexParameter++, categoria1);

					pStmt2.setString(indexParameter++, categoria5);
					pStmt2.setString(indexParameter++, categoria4);
					pStmt2.setString(indexParameter++, categoria3);
					pStmt2.setString(indexParameter++, categoria2);

					pStmt2.setString(indexParameter++, categoria5);
					pStmt2.setString(indexParameter++, categoria4);
					pStmt2.setString(indexParameter++, categoria3);

					pStmt2.setString(indexParameter++, categoria5);
					pStmt2.setString(indexParameter++, categoria4);
					
					pStmt2.setString(indexParameter++, categoria4);
	            } else {
					pStmt2.setString(indexParameter++, categoria5);
					pStmt2.setString(indexParameter++, categoria4);
					pStmt2.setString(indexParameter++, categoria3);
					pStmt2.setString(indexParameter++, categoria2);
					pStmt2.setString(indexParameter++, categoria1);

					pStmt2.setString(indexParameter++, categoria1);
					pStmt2.setString(indexParameter++, categoria2);
					pStmt2.setString(indexParameter++, categoria3);
					pStmt2.setString(indexParameter++, categoria4);

					pStmt2.setString(indexParameter++, categoria1);
					pStmt2.setString(indexParameter++, categoria2);
					pStmt2.setString(indexParameter++, categoria3);
					
					pStmt2.setString(indexParameter++, categoria1);
					pStmt2.setString(indexParameter++, categoria2);

					pStmt2.setString(indexParameter++, categoria1);
	            }
	        }

            if( recommenderMode == RecommenderMode.SAME_PRICE || recommenderMode == RecommenderMode.DIFFERENT_PRICE )
				pStmt2.setFloat(indexParameter++, preco_referencia);
			else if( recommenderMode == RecommenderMode.GEO_LOCATION ) { 
				pStmt2.setString(indexParameter++, pais);
				pStmt2.setString(indexParameter++, estado);
				pStmt2.setString(indexParameter++, cidade);

				pStmt2.setString(indexParameter++, pais);
				pStmt2.setString(indexParameter++, estado);

				pStmt2.setString(indexParameter++, pais);
			} else if ( recommenderMode == RecommenderMode.GEO_LOCATION_DIFFERENT_CITY ) { 
				pStmt2.setString(indexParameter++, pais);
				pStmt2.setString(indexParameter++, estado);
				pStmt2.setString(indexParameter++, cidade);

				pStmt2.setString(indexParameter++, pais);
				pStmt2.setString(indexParameter++, estado);
				pStmt2.setString(indexParameter++, cidade);

				pStmt2.setString(indexParameter++, pais);
				pStmt2.setString(indexParameter++, estado);

				pStmt2.setString(indexParameter++, pais);

			}
			
			ResultSet res2 = pStmt2.executeQuery();
			def recomendacoes = res.getString(3);
			if(recomendacoes==null) recomendacoes = "";
			def cnt = 0;
			while( res2.next() ) {
				recomendacoes += (recomendacoes==""?"":",") + res2.getString(1);
				cnt++;
			}
			ConnHelper.closeResources(pStmt2, null);

			PreparedStatement pStmt3 = conn.prepareStatement("update catalogo set " + recommenderField + " = ifnull(?, " + recommenderField + "), versao = ? where id = ? ");
			pStmt3.setString(1, recomendacoes);
			pStmt3.setLong(2, (long)System.currentTimeMillis()/1000);
			pStmt3.setLong(3, res.getLong(2));
			def i = pStmt3.executeUpdate();
			if(i>0) println("COMPLETOU recomendacao para id: " + res.getLong(2) + " -add: " + (total) + "/" + cnt);
			ConnHelper.closeResources(pStmt3, null);
		}
		ConnHelper.closeResources(pStmt, null);
		print("\ndropando tabela temporaria ...");
                pTmp = conn.prepareStatement("""drop table catalogo_tmp""");
                pTmp.executeUpdate();
		ConnHelper.closeResources(pTmp, null);

		println("OK");
	}

	public static void validacao (Connection conn) {
		validacao( conn, 1000, 15.0);
	}
	public static void validacao (Connection conn, int avgProducts, double minActivePercentage) {
		def ativos;
		def inativos;
		def naoRecomendados;

		PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where ativo = '1'");
		ResultSet res = pStmt.executeQuery();
		if( res.next() )
			ativos = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select count(*) from catalogo where ativo = '0'");
		res = pStmt.executeQuery();
		if( res.next() )
			inativos = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select count(*) from catalogo where recomendacoes is null or recomendacoes = ''");
		res = pStmt.executeQuery();
		if( res.next() )
			naoRecomendados = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
		
		double percentualAtivo = (double) ( ( ativos * 100 ) / ( ativos + inativos) );
		double percentualSemRecomendacao = (double) ( ( naoRecomendados * 100 ) / ( ativos + inativos) );
		
		if(percentualAtivo < minActivePercentage )
			throw new RuntimeException("erro de inconsistencia [percentual ativo " + percentualAtivo + "]... abortando atualizacao");
		if(percentualSemRecomendacao > minActivePercentage )
			throw new RuntimeException("erro de inconsistencia [percentual sem recomendacao " + percentualSemRecomendacao + "]... abortando atualizacao");
		if(ativos < avgProducts )
			throw new RuntimeException("erro de inconsistencia [produtos ativos " + ativos + "]... abortando atualizacao");
		println("Validado !!!");
	}

}
