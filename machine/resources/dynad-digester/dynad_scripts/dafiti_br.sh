echo "INICIO - Dafiti BR"

date
START=$(date +%s)

### PARAMETROS ###
echo $1
echo $2
XML_URL=http://parceiro.dafiti.com.br/xml/xml_asapcode.xml
XML_FILE=/mnt/TMP/xml/dafiti.xml
TEMP_NAME=dftbr
SCHEMA_IN=dynad_dafiti
SCHEMA_OUT=dynad_dafiti_sn
DB_HOST=d3-asap-cdn6
EXP_HOST="d3-asap-cdn5 d3-asap-cdn6"
DIGESTER=/root/dynad_scripts/DigesterDafitiBR.groovy
SNFILE=/mnt/TMP/sna-files/dafiti.txt
SNSERVER="200.147.166.24 200.147.166.25 200.147.166.26 200.147.166.27 d3-asap-supernova-fe5"
SNDATASOURCE=teste
#DIGESTER=/root/dynad_scripts/DigesterDafiti.groovy
##################


RANKING_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/sqls/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old.6
mv $XML_FILE.old.5 $XML_FILE.old.6
mv $XML_FILE.old.4 $XML_FILE.old.4
mv $XML_FILE.old.3 $XML_FILE.old.3
mv $XML_FILE.old.2 $XML_FILE.old.2
mv $XML_FILE.old.1 $XML_FILE.old.2
mv $XML_FILE.old $XML_FILE.old.1
mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old
rm $EXPORT_FILE.old
mv $EXPORT_FILE $EXPORT_FILE.old

if [ -e "/mnt/TMP/sna-files/dafiti_br_uolcliques.txt" ]; then
    rm /mnt/TMP/sna-files/dafiti_br_uolcliques.txt
    echo "removed /mnt/TMP/sna-files/dafiti_br_uolcliques.txt"
fi

if [ -e "/mnt/TMP/sna-files/dafiti_br_uolcliques.zip" ]; then
    rm /mnt/TMP/sna-files/dafiti_br_uolcliques.zip
    echo "removed /mnt/TMP/sna-files/dafiti_br_uolcliques.zip"
fi

if [ -e "/mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.zip" ]; then
    rm /mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.zip
    echo "removed /mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.zip"
fi

if [ -e "/mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt" ]; then
    rm /mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt
    echo "removed /mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt"
fi


echo "baixando xml ..."
wget $XML_URL -O $XML_FILE

echo "verificando xml ..."
size=`stat -c %s $XML_FILE`

if [ $size -lt 5000000 ]
then
echo "invalid file size"
exit 1
fi

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA_OUT --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA_IN < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA_IN < /root/dynad_scripts/ranking.sql
fi




cmp -s $XML_FILE $XML_FILE.old >/dev/null
if [ $? -ne 0 ]
then

#  echo "exportando categorias acessadas ..."
#  mysqldump -u digester -pretsegid -h $DB_HOST $SCHEMA_OUT pre_category > /mnt/TMP/exp_categs_$TEMP_NAME.sql

#  echo "importando categorias acessadas ..."
#  mysql -u dynad -pdanyd $SCHEMA_IN < /mnt/TMP/exp_categs_$TEMP_NAME.sql

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA_IN < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA_IN top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
     echo "exportando para $ip"
     mysql -u digester -pretsegid $SCHEMA_OUT -h $ip < $EXPORT_FILE
  done

  echo "atualizando supernova 2 ..."
  for ip in $SNSERVER; do
     echo "exportando para $ip"
     curl --header 'Host: digester.dynad.net' -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
  done
 
  sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 0 1296000 teste || exit 1

  echo "forcando sincronismo do supernova 1"
  sh /root/synch_watcher/force_sync_dafiti.sh

  if [ -s "/mnt/TMP/sna-files/dafiti_br_uolcliques.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip dafiti_br_uolcliques.zip dafiti_br_uolcliques.txt

        echo "starting sync process"
        curl -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi

   if [ -s "/mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip dafiti_br_uolcliques_topsellers.zip dafiti_br_uolcliques_topsellers.txt

        echo "starting sync process"
        curl -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques_topsellers.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques_topsellers.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi

else

  if [ $RANKING -eq 1 ]
  then

    echo "executando script de importacao ..."
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA_IN top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
       echo "exportando para $ip"
       mysql -u digester -pretsegid $SCHEMA_OUT -h $ip < $EXPORT_FILE
    done

    echo "atualizando supernova 2 ..."
    for ip in $SNSERVER; do
       echo "exportando para $ip"
       curl --header 'Host: digester.dynad.net' -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
    done

    echo "forcando sincronismo do supernova 1"
    sh /root/synch_watcher/force_sync_dafiti.sh

    if [ -s "/mnt/TMP/sna-files/dafiti_br_uolcliques.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip dafiti_br_uolcliques.zip dafiti_br_uolcliques.txt

        echo "starting sync process"
        curl -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi

   if [ -s "/mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip dafiti_br_uolcliques_topsellers.zip dafiti_br_uolcliques_topsellers.txt

        echo "starting sync process"
        curl -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques_topsellers.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=dafiti_br -F file=@dafiti_br_uolcliques_topsellers.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi
  else

    echo "*** nada a fazer ..."

  fi

fi

date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos"
echo "FIM***"

