import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class Spider {
	static int key;
	static {
		ConnHelper.schema = "dynad_fastshop";
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
		ResultSet res = pStmt.executeQuery();
		if (res.next()) key = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
	}

	public static boolean _DEBUG_ = false;


	private static String getUncodedString(String conteudo) {
		java.util.regex.Pattern myPattern = java.util.regex.Pattern.compile("&#([0-9]+);");
		java.util.regex.Matcher myMatcher = myPattern.matcher(conteudo);
		while (myMatcher.find()) {
			int asciiCode = Integer.parseInt(myMatcher.group().replaceAll("[^0-9]", ""));
			conteudo = conteudo.replaceAll(myMatcher.group(), String.valueOf((char) asciiCode));
		}
		return conteudo;
	}

	private static markXml(String file) {
		Connection conn = ConnHelper.get().reopen();
		int cline = 0;
		def p = null;
		String line = null;

		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

		def summary = new XmlParser().parse(file);
		def list = summary.Produto

		list.each {
			/*
			<FASTSHOP>
			<Timestamp>2013-11-11T15:52:17 GMT-3</Timestamp><Produto>
			<Codigo>TR21147040</Codigo>
			<Descricao>Faca Mezzaluna em Inox Originale Polywwod - Tramontina Design Collection - 21147040  </Descricao>
			<PrecoDe>119,00</PrecoDe>
			<PrecoPor>119,00</PrecoPor>
			<Parcelas>2</Parcelas>
			<PrecoAVista>115,88</PrecoAVista>
			<Disponibilidade>Imediata</Disponibilidade>
			<Imagem_Pequena>http://www1.fastshop.com.br/imgprod/TR21147040_P.JPG</Imagem_Pequena>
			<Imagem_Grande>http://www1.fastshop.com.br/imgprod/NA620_g.jpg,NA620_g1.jpg,NA620_g2.jpg,NA620_g3.jpg,NA620_g4.jpg</Imagem_Grande>
			<Categoria>Facas/Cepos</Categoria>
			<Link><![CDATA[http://www.fastshop.com.br/loja/faca-mezzaluna-em-inox-originale-tramontina-21147040/?partner=parceiro-asapcode]]></Link>
			<LinkRastreamento><![CDATA[http://www.fastshop.com.br/loja/faca-mezzaluna-em-inox-originale-tramontina-21147040/?partner=parceiro-asapcode&cm_mmc=xml_parceiro-asapcode-_-ND-_-Facas%2fCepos-_-TR21147040&utm_source=xml_parceiro-asapcode&utm_medium=xml&utm_content=Facas%2fCepos&utm_campaign=parceiro-asapcode&utm_term=faca-mezzaluna-em-inox-originale-tramontina-21147040]]></LinkRastreamento>
			<Fabricante>ACER/ACTIVA</Fabricante>
			</Produto>
			*/
			sku = it.Codigo.text()
			nome = Digester.stripTags(Digester.ISO2UTF8(it.Descricao.text()));
			if (nome.indexOf('&#') > -1) nome = getUncodedString(nome);

			def pp1;
			pp1 = nome.indexOf("-");
			if (pp1 > 24) nome = nome.substring(0, pp1);
			pp1 = nome.indexOf(",");
			if (pp1 > 24) nome = nome.substring(0, pp1);
			pp1 = nome.indexOf(".");
			if (pp1 > 24) nome = nome.substring(0, pp1);
			pp1 = nome.indexOf("/");
			if (pp1 > 24) nome = nome.substring(0, pp1);
						
			url = it.LinkRastreamento.text()
			if (url.indexOf("?") > -1) url = url.substring(0, url.indexOf("?"));
			if (url.endsWith("/")) url = url.substring(0, url.length() - 1);

			fprice = Digester.autoNormalizaMoeda(it.PrecoAVista.text(), false, new Locale("pt", "BR"));
			oprice = Digester.autoNormalizaMoeda(it.PrecoDe.text(), false, new Locale("pt", "BR"));
			nparcelas = it.Parcelas.text();
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
			vparcelas = Digester.autoNormalizaMoeda("" + df.format(Float.parseFloat(Digester.autoNormalizaMoeda(it.PrecoPor.text(), true, new Locale("en", "US"))) / Integer.parseInt(nparcelas)), false, new Locale("pt", "BR"));

			image = it.Imagem_Normal.text().replace('_N', '_G')
			if (image.indexOf(",") > -1) image = image.substring(0, image.indexOf(","));

			c1 = it.Categoria.text()
			c2 = ""
			c3 = ""

			marca = it.Fabricante.text()

			fprice = fprice == null || fprice == "" ? "0,00" : fprice;
			oprice = oprice == null || oprice == "" ? "0,00" : oprice;
			vparcelas = vparcelas == null || vparcelas == "" ? "0,00" : vparcelas;			
			Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false);
			if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
		}

	}

	static String getField(String buffer) {
		def pi; // =  buffer.indexOf( '<![CDATA[' );
		def pf; // =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";
		while ((pi = buffer.indexOf('<![CDATA[')) != -1 && (pf = buffer.indexOf(']]>')) != -1 && pi < pf) {
			entrou = true;
			if (pi + 9 < pf) {
				retorno += buffer.substring(pi + 9, pf);
			}
			buffer = buffer.substring(pf + 3);
		}
		if (!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {
				retorno = buffer.substring(pi + 1, pf);
			} catch (Exception ex) {
				println(buffer);
				ex.printStackTrace();
			}
		}
		return retorno.replace("%20", " ");
	}

	public static void main(String[] args) {
		if (args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml('/mnt/TMP/xml/fastshop.xml');
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.validacao(ConnHelper.get().reopen(), 200);
		} else if (args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
		}

		///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, ativo, nome, link,  imagem, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/fastshop.txt").withWriter { out->
        	//ESCREVE O HEADER
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            //ESCREVE OS DADOS
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }

		///////// EXPORT UOL CLIQUES ///////////
		java.util.List < String > listTopSellers = new java.util.ArrayList();
		conn = ConnHelper.get().reopen();
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes, categoria1 from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/fastshop_uolcliques.txt").withWriter { out->
			out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while (res.next()) {
				out.print(res.getString(1) + "|FastShop|" + res.getString(5) + "|");
				if (res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1) out.print('A partir de R$ ' + res.getString(8));
				else out.print(res.getString(9) + 'x de R$ ' + res.getString(10));
				out.print('|' + res.getString(6) + '?partner=parceiro-barrauol&cm_mmc=dsp_uol-_-' + getStringForTracking(res.getString(12)) + '-_-uol_cliques-' + getStringForTracking(res.getString(1)) + '&utm_source=dsp_uol&utm_medium=uol_cliques&utm_term=' + getStringForTracking(res.getString(1)) + '&utm_campaign=uol_cliques_rt|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3)) + '|' + res.getString(11) + '\n');
				out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
				if (listTopSellers.size() < 10) listTopSellers.add(res.getString(1));
			}
		}
		ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select a.sku from catalogo a join best_sellers b on a.id_sku = b.id_sku where a.ativo = '1' order by b.pos");
		res = pStmt.executeQuery();
		int numBsDb = 0;
		new File("/mnt/TMP/sna-files/fastshop_uolcliques_topsellers.txt").withWriter { out ->
			while (res.next()) {
				out.println(res.getString(1));
				numBsDb++;
			}
			if (numBsDb < 20) {
				listTopSellers.each {
					itTS -> out.println(itTS);
				}
			}
		}
		ConnHelper.closeResources(pStmt, res);
	}

	private static String removeAcentos(String string) {
		string = string.replaceAll("[ÂÀÁÄÃ]", "A");
		string = string.replaceAll("[âãàáä]", "a");
		string = string.replaceAll("[ÊÈÉË]", "E");
		string = string.replaceAll("[êèéë]", "e");
		string = string.replaceAll("ÎÍÌÏ", "I");
		string = string.replaceAll("îíìï", "i");
		string = string.replaceAll("[ÔÕÒÓÖ]", "O");
		string = string.replaceAll("[ôõòóö]", "o");
		string = string.replaceAll("[ÛÙÚÜ]", "U");
		string = string.replaceAll("[ûúùü]", "u");
		string = string.replaceAll("Ç", "C");
		string = string.replaceAll("ç", "c");
		string = string.replaceAll("[ýÿ]", "y");
		string = string.replaceAll("Ý", "Y");
		string = string.replaceAll("ñ", "n");
		string = string.replaceAll("Ñ", "N");
		return string;
	}

	private static String getStringForTracking(String conteudo) {
		if (conteudo == null) return null;
		conteudo = removeAcentos(conteudo);
		conteudo = Normalizer.normalize(conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]", "-").toLowerCase();
	}
}