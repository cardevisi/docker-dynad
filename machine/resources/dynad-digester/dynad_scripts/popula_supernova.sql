DELIMITER ||

CREATE PROCEDURE `popula_supernova`()
BEGIN

   DECLARE done INT DEFAULT FALSE;
   DECLARE v_id int(8);
   DECLARE v_sku varchar(45);
   DECLARE v_categoria1 varchar(256);
   DECLARE v_categoria2 varchar(256);
   DECLARE v_categoria3 varchar(256);
   DECLARE cnt int(8);
   DECLARE v_id_sku int;
   DECLARE v_id_categoria int;
   DECLARE v_key int;
   DECLARE v_versao bigint(19);
	
	SET v_versao = UNIX_TIMESTAMP();
	-- remove blank records
	UPDATE catalogo SET 
		categoria1 = IFNULL(categoria1, 'empty'), 
		categoria2 = IFNULL(categoria2, 'empty'), 
		categoria3 = IFNULL(categoria3, 'empty') 
	WHERE
		categoria1 is null or categoria2 is null or categoria3 is null;
	
	-- create new records
	INSERT INTO codigos_categorias (categoria, categoria1, categoria2, categoria3, versao)
	SELECT DISTINCT 
		concat(concat(concat(concat(IFNULL(categoria1,'empty'), '/'),IFNULL(categoria2,'empty')), '/'),IFNULL(categoria3,'empty')), 
		categoria1, 
		categoria2, 
		categoria3,
		v_versao
	FROM catalogo 
	WHERE 
		id_categoria is null and 
		(IFNULL(categoria1,'empty'), IFNULL(categoria2,'empty'), IFNULL(categoria3,'empty')) 
		not in (select distinct categoria1, categoria2, categoria3 from codigos_categorias);
	
	-- link new records
	update 
		catalogo a 
	set 
		a.persisted_categoria1 =ifnull(a.categoria1, 'empty'), 
		a.persisted_categoria2 =ifnull(a.categoria2, 'empty'), 
		a.persisted_categoria3 =ifnull(a.categoria3, 'empty'), 
		a.id_categoria = (select b.id from codigos_categorias b where b.categoria1 = ifnull(a.categoria1, 'empty') 
			and b.categoria2 = ifnull(a.categoria2, 'empty')
			and b.categoria3 = ifnull(a.categoria3, 'empty') )
	where 
		a.id_categoria is null;
	
	-- create new records
	INSERT INTO codigos_categorias (categoria, categoria1, categoria2, categoria3, versao)
	SELECT DISTINCT 
		concat(concat(concat(concat(IFNULL(categoria1,'empty'), '/'),IFNULL(categoria2,'empty')), '/'),IFNULL(categoria3,'empty')), 
		categoria1, 
		categoria2, 
		categoria3,
		v_versao
	FROM catalogo 
	WHERE 
		id_categoria is not null 
		and ( persisted_categoria1 <> IFNULL(categoria1,'empty') or persisted_categoria2 <> IFNULL(categoria2,'empty') or persisted_categoria3 <> IFNULL(categoria3,'empty') )
		and (IFNULL(categoria1,'empty'), IFNULL(categoria2,'empty'), IFNULL(categoria3,'empty')) 
		not in (select distinct categoria1, categoria2, categoria3 from codigos_categorias);
	
	-- link changed records
	update 
		catalogo a 
	set 
		a.persisted_categoria1 =ifnull(a.categoria1, 'empty'), 
		a.persisted_categoria2 =ifnull(a.categoria2, 'empty'), 
		a.persisted_categoria3 =ifnull(a.categoria3, 'empty'), 
		a.id_categoria = (select b.id from codigos_categorias b where b.categoria1 = ifnull(a.categoria1, 'empty') 
			and b.categoria2 = ifnull(a.categoria2, 'empty')
			and b.categoria3 = ifnull(a.categoria3, 'empty') )
	where 
		a.id_categoria is not null 
		and ( a.persisted_categoria1 <> IFNULL(a.categoria1,'empty') 
				or a.persisted_categoria2 <> IFNULL(a.categoria2,'empty') 
				or a.persisted_categoria3 <> IFNULL(a.categoria3,'empty') );	
	
	-- ajusta id da tabela catalogo
   UPDATE catalogo a SET a.versao = unix_timestamp(), a.id_sku = a.id WHERE a.id_sku <> a.id or a.id_sku is null;	
END

||

DELIMITER ;

