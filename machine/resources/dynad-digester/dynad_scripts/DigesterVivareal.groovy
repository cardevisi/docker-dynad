import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

public class DigesterVivareal {
    static int key;
    static {
        ConnHelper.schema = "dynad_vivareal";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if (res.next()) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }
    public DigesterVivareal(){ }    
    public markXml(String file) {
        try {            
            SAXParserFactory parserFactor = SAXParserFactory.newInstance();
            SAXParser parser = parserFactor.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {                                
                Connection conn = ConnHelper.get().reopen();
                int cline = 0;        
                boolean jaTemImagem = false;        
                String sku, nome, descricao, link, preco, imagem, categoria, cidade, estado, bairro, latitude, longitude, data, tagAtual;
                public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
                    tagAtual = qName;
                }                
                public void characters(char[] ch, int start, int length) throws SAXException {
                    String valor = new String(ch, start, length).trim();                                        
                    if (tagAtual.equals("id")) {
                        sku = valor;
                        jaTemImagem = false;   
                    }
                    else if (tagAtual.equals("url")) link = valor;
                    else if (tagAtual.equals("title")) nome = valor;
                    else if (tagAtual.equals("content")) descricao = valor.replaceAll(/\t/, '').replaceAll(/\n/, '<br>').replaceAll('  ', '');
                    else if (tagAtual.equals("property_type")) categoria = valor;                                        
                    else if (tagAtual.equals("price")) preco = Digester.autoNormalizaMoeda(valor, false, new Locale("pt", "BR"));
                    else if (tagAtual.equals("region")) estado = valor;
                    else if (tagAtual.equals("city")) cidade = valor;                    
                    else if (tagAtual.equals("city_area")) bairro = valor;
                    else if (tagAtual.equals("latitude")) latitude = valor;                    
                    else if (tagAtual.equals("longitude")) longitude = valor;
                    else if (tagAtual.equals("picture_url")) {
                        if (!jaTemImagem) {
                            imagem = valor;   
                            jaTemImagem = true;                            
                        }                        
                    }                    
                    tagAtual = "";
                }                
                public void endElement(String uri, String localName, String qName) throws SAXException {                    
                    if (qName.equals('ad')) {
                        FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
                        FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
                        java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
                        fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: link, lookupChange: true, platformType: 'LINK') );
                        fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: imagem, lookupChange: true, platformType: 'IMG') );                    
                        fields.add( new FieldMetadata(columnName : 'estado', columnType: 'string', columnValue: estado, lookupChange: true, platformType: 'NA') );                    
                        fields.add( new FieldMetadata(columnName : 'cidade', columnType: 'string', columnValue: cidade, lookupChange: true, platformType: 'NA') );                    
                        fields.add( new FieldMetadata(columnName : 'bairro', columnType: 'string', columnValue: bairro, lookupChange: true, platformType: 'NA') );                                        
                        fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );                    
                        fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: preco, lookupChange: true, platformType: 'NA') );                    
                        fields.add( new FieldMetadata(columnName : 'latitude', columnType: 'string', columnValue: latitude, lookupChange: true, platformType: 'NA') );                    
                        fields.add( new FieldMetadata(columnName : 'longitude', columnType: 'string', columnValue: longitude, lookupChange: true, platformType: 'NA') );                    
                        DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);           
                        if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
                    }                    
                }                                
            };                        
            parser.parse(file, handler);
        } catch(Exception ex) {
            print ex;
            ex.getStackTrace();
        }
    }

    public static void main(String[] args){
        if (args.length == 0 || args[0] != 'BEST_SELLERS') {
            new DigesterVivareal().markXml('/mnt/TMP/xml/vivareal_1.xml');
            //new DigesterVivareal().markXml('/mnt/TMP/xml/vivareal_2.xml');
            //new DigesterVivareal().markXml('/mnt/TMP/xml/vivareal_3.xml');
            //DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
            //DigesterV2.completaRecomendacoes(ConnHelper.get().reopen(), false);
            //DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
        } else if (args[0] == 'BEST_SELLERS') {
            //DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
            //DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
        }

        ///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, descricao, link, preco_promocional, estado, cidade, bairro, latitude, longitude, categoria, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and descricao is not null and preco_promocional is not null and latitude is not null and longitude is not null and categoria is not null and estado is not null and cidade is not null and bairro is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/vivareal.txt").withWriter {
            out ->
            for (int i = 1; i <= rsmd.getColumnCount(); i++) out.print((i > 1 ? "|" : "metadata|") + rsmd.getColumnName(i));
            out.print("\n");
            while (res.next()) {
                out.print(res.getString(1) + "|" + res.getString(2));
                out.print("|http://static.dynad.net/let/" + URLCodec.encrypt(res.getString(3)));
                for (int i = 4; i <= rsmd.getColumnCount(); i++) {
                    if (res.getString(i)) out.print("|" + res.getString(i).replace("|", " "));  
                }
                out.print("\n");
                out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);
        println 'SN2 - DATA GENERATED';
    }        
}
