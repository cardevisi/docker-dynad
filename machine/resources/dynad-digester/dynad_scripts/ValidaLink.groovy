import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;

import java.net.HttpURLConnection;
import java.net.URL;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class ValidaLink {
	static boolean verbose = false;
	
	public static Boolean valida(String finalUrl) {
		URL url = null;
		HttpURLConnection con = null;
		Set header = null;
		int statusCode = -1;

		try {
			url = new URL(finalUrl);
			HttpURLConnection.setFollowRedirects(true);
			con = (HttpURLConnection) url.openConnection();
		} catch(Exception ex) {
			throw new Exception("Invalid url; [" + finalUrl + "] : " + ex.getMessage());
		}
					
		try {
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setUseCaches(false);

			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8");

		
			con.connect();
			
			header = con.getHeaderFields().entrySet();
		} catch (java.io.FileNotFoundException ex) {
			throw new Exception("File not found; [" + finalUrl + "]");
		} catch (Exception ex) {
			if(verbose) ex.printStackTrace();
			throw new Exception("Error retrieving url; [" + finalUrl + "]");
		}

		if( con.getURL().toString().indexOf("Erro.aspx") > -1 ) { 
			if( verbose )
				println("******** FALHA URL[" + finalUrl + "] -> [" + con.getURL().toString() + "]" );
			throw new Exception("******** FALHA URL[" + finalUrl + "] -> [" + con.getURL().toString() + "]");
		}
					
		try {
			statusCode = con.getResponseCode();
		} catch (java.io.FileNotFoundException ex) {
			throw new Exception("File not found; [" + finalUrl + "]");
		} catch (Exception ex) {
			if(verbose) ex.printStackTrace();
			throw new Exception("Error processing url; [" + finalUrl + "]");
		} finally {
			try { con.disconnect(); } catch(Exception e) {if(verbose) System.out.println(e.getMessage());}
		}

		if(statusCode == 200) {
			if(verbose) print("status:[" + statusCode + "] : ");
			if(header != null) {
				if(verbose) println(header);
			} else {
				if(verbose) println("none");
			}
		
			return true;
		} else {
			if(verbose) println("ERROR");
			return false;
		}
	}

        public static void main (String [] args ) {
		if(args.size() < 2) {
			println("groovy ValidaLink SCHEMA COLUMN");
			return ;
		}

                ConnHelper.schema = args[0];
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select " + args[1] + " from catalogo where ativo = '1'");
                ResultSet res = pStmt.executeQuery();

		String finalUrl;
		boolean ret;
		float sucesso = 0;
		float erro = 0;

                while( res.next() ) {
                        finalUrl = res.getString(1);
			try {ret = valida(finalUrl);}catch(Exception e){if(verbose) println(e.getMessage()); ret = false;}
			if(ret) { print(">"); sucesso++; } else { print("\n"+finalUrl+"\n"); erro++; }
		}
                ConnHelper.closeResources(pStmt, res);
		println("\n\nresults:\nsucesso:" + Math.round((sucesso/(sucesso+erro))*100)/100 + "\nerros:" + Math.round((erro/(sucesso+erro))*100)/100);
	}

}
