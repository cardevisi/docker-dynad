import groovy.sql.Sql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


class ConnHelper {
	public static String host = "localhost";
	public static String schema = "";
	public static String usuario = "root";
	public static String senha = "";
	
	private java.sql.Connection conn;
	private static ConnHelper instance = null;
	
	public ConnHelper () { throw new UnsupportedOperationException("constructor not available."); }
	private ConnHelper ( int x ) {
		conn = reopen();
	}
	
	public Connection reopen () { 
		if( conn != null ) {
			conn.close();
			conn = null;
		}
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://"+host+"/"+schema+"?user="+usuario+"&password=" + senha + "&characterSetResults=UTF8&noAccessToProcedureBodies=true");
	} 
	
	public static ConnHelper get () {
		if( instance == null )
			instance = new ConnHelper( 1 );
		return instance;
	}
	
	public static closeResources ( Connection conn ) {
		try { if( conn != null ) conn.close(); } catch ( Exception ex) {}
	}
	
	public static closeResources ( PreparedStatement pStmt ) {
		try { if( pStmt != null ) pStmt.close(); } catch ( Exception ex) {}
	}
	
	public static closeResources ( PreparedStatement pStmt, ResultSet res ) {
		try { if( res != null ) res.close(); } catch ( Exception ex) {} 
		try { if( pStmt != null ) pStmt.close(); } catch ( Exception ex) {}
	}
	
	
}

