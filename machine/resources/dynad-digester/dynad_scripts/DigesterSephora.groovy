import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class Spider {
	static int key;

	static final String ID_PARCERIA = "28015";

	static {
		ConnHelper.schema = "dynad_sephora";
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
		ResultSet res = pStmt.executeQuery();
		if (res.next()) key = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
	}

	public static boolean _DEBUG_ = false;

	private static markXml(String file) {

		Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;

		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

		def summary = new XmlParser().parse(file);
		def list = summary.produto

		/*
		<produto>
			<id_produto>2</id_produto>
			<descricao>4711 Original Eau de Cologne 50 ml de 4711</descricao>
			<precode>79</precode>
			<preco_normal>79</preco_normal>
			<parcelamento>ou 7x de R$11.29 sem juros.</parcelamento>
			<categoria>Perfumes\Masculino</categoria>
			<link_produto>http://www.sephora.com.br/site/produto.asp?id=2&amp;idparceria=</link_produto>
			<imagem>http://imagens.sephora.com.br/{BCE78BA1-3887-443A-8622-31F63DA60B68}_4711-225px.jpg</imagem>
			<marca>4711</marca>
			<avaliacao>4.50</avaliacao>
			<id_produtoPai>2</id_produtoPai>
		</produto>

		<produto>
			<id_produto>2</id_produto>
			<gtin>4011700740383</gtin>
			<descricao>4711 Original Eau de Cologne 50 ml</descricao>
			<link_produto>http://www.sephora.com.br/4711/perfumes/masculino/4711-original-eau-de-cologne-2</link_produto>
			<preco_normal>79</preco_normal>
			<precode>79</precode>
			<imagem>http://imagens.sephora.com.br/{BCE78BA1-3887-443A-8622-31F63DA60B68}_4711-225px.jpg</imagem>
			<categoria>Perfumes\Masculino</categoria>
			<marca>4711</marca>
			<avaliacao>4.50</avaliacao>
			<id_produtoPai>2</id_produtoPai>
			<parcelamento>7</parcelamento>
			<valor_parcela>11.29</valor_parcela>
		</produto>
		*/

		java.util.List <String> listaCodigosPai = new java.util.ArrayList < String > ();
		list.each {
			sku = it.id_produto.text()
			nome = Digester.stripTags(Digester.ISO2UTF8(it.descricao.text()))
			url = it.link_produto.text()

			if (url.indexOf("&amp;") > -1) url = url.substring(0, url.indexOf("&amp;"));

			if (url.indexOf("idparceria") > -1) url = url.substring(0, url.indexOf("idparceria"));

			if (url.indexOf('?') > -1) {
				if (url.charAt(url.length() - 1) != '&') url += "&";
			} else url += '?';
			url += "idparceria=" + ID_PARCERIA;

			def t;
			fprice = it.preco_normal.text()
			oprice = it.precode.text()
			fprice = Digester.autoNormalizaMoeda(fprice, false, new Locale("pt", "BR"))
			oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"))
			nparcelas = '';
			vparcelas = '';

			def tmp;

			if (it.parcelamento != null && it.parcelamento.text() != '' && it.valor_parcela != null && it.valor_parcela.text() != '') {
				nparcelas = it.parcelamento.text();
				vparcelas = it.valor_parcela.text();
				vparcelas = Digester.autoNormalizaMoeda(vparcelas, false, new Locale("pt", "BR"))
			} else {
				tmp = it.parcelamento.text()
				if ((t = tmp.indexOf("x")) > -1) {
					def num = tmp.substring(0, t).replace("ou", "").trim();
					def resto = tmp.substring(t + 1).trim();
					def p2 = resto.indexOf(" ");
					if (p2 > 0) resto = resto.substring(p2).trim();
					p2 = resto.indexOf(" ");
					if (p2 > 0) resto = resto.substring(0, p2).trim();
					nparcelas = num;
					vparcelas = resto
					if ((t = vparcelas.indexOf('R$')) > -1) {
						vparcelas = Digester.autoNormalizaMoeda(vparcelas.substring(t + 2), false, new Locale("pt", "BR"));
					}
				}
			}

			image = it.imagem.text()
			if ((t = image.indexOf("sephora.com.br/")) > -1) {
				t += "sephora.com.br/".length()
				def img1 = image.substring(0, t)
				if (t + 1 < image.length()) {
					def img2 = image.substring(t)
					image = img1 + img2
				}
			}

			desconto = it.discount.text()
			tmp = it.categoria.text()
			t = 0;
			tmp.split("\\\\").each {
				if (t == 0) c1 = it
				if (t == 1) c2 = it
				if (t == 2) c3 = it
				t++
			}

			c3 = it.id_produtoPai.text()
			marca = it.marca.text();
			String ativo = it.availability.text() == 'in stock' ? '1' : '0';
			Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, ativo, false);

			if (c3 != null && c3 != '' && listaCodigosPai.indexOf(c3) == -1) {
				Digester.salvaCatalogo(conn, c3, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, ativo, false);
				listaCodigosPai.add(c3);
			}		
			if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
		}
	}	

	public static void main(String[] args) {		
    	if(args.length == 0 || args[0] != 'DIFFERENT_NAMELINK') {			
			markXml( '/mnt/TMP/xml/sephora.xml' );
            Digester.normalizaSupernova(ConnHelper.get().reopen());
            Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
            Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.DIFFERENT_NAME);
            Digester.bestSellers(ConnHelper.get().reopen());            
            Digester.validacao(ConnHelper.get().reopen());
        } else {
        	if(args[0] == 'BEST_SELLERS') Digester.bestSellers(ConnHelper.get().reopen());
        }

        ///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/sephora.txt").withWriter { out ->
            for(int i=3; i<=rsmd.getColumnCount(); i++) out.print((i>3?"|":"metadata|sku|")+rsmd.getColumnName(i));
            out.print("\n");
            while( res.next() ) {
                out.print(res.getString(1));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }                
        }
        ConnHelper.closeResources(pStmt, res);

		///////// EXPORT UOL CLIQUES ///////////
		java.util.List < String > listTopSellers = new java.util.ArrayList();
		conn = ConnHelper.get().reopen();
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/sephora_uolcliques.txt").withWriter {
			out -> out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while (res.next()) {
				out.print(res.getString(1) + "|Sephora|" + res.getString(5) + "|");
				if (res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1) out.print('A partir de R$ ' + res.getString(8));
				else out.print(res.getString(9) + 'x de R$ ' + res.getString(10));
				out.print('|' + getValidParameters(res.getString(6), new String[0], true) + 'utm_source=uol&utm_medium=uolcliques&utm_campaign=20150825_UolCliques|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3)) + '|' + res.getString(11) + '\n');
				out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
				if (listTopSellers.size() < 10) listTopSellers.add(res.getString(1));
			}
		}
		ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select a.sku from catalogo a join best_sellers b on a.id_sku = b.id_sku where a.ativo = '1' order by b.pos");
		res = pStmt.executeQuery();
		int numBsDb = 0;
		new File("/mnt/TMP/sna-files/sephora_uolcliques_topsellers.txt").withWriter {
			out ->
			while (res.next()) {
				out.println(res.getString(1));
				numBsDb++;
			}
			if (numBsDb < 20) {
				listTopSellers.each {
					itTS -> out.println(itTS);
				}
			}
		}
		ConnHelper.closeResources(pStmt, res);
	}

	private static String getValidParameters(String productURL, String[] arrSkipParameters, boolean addDel) {
		StringBuilder sb = new StringBuilder();
		String urlParam = productURL.indexOf('?') > -1 ? (productURL.substring(productURL.indexOf('?') + 1)) : "";
		if (urlParam.trim().equals("")) {
			if (addDel) {
				if (productURL.indexOf('?') > -1 && (!productURL.endsWith('&') && !productURL.endsWith('?'))) productURL += '&';
				else if (productURL.indexOf('?') == -1) productURL += '?';
			}
			return productURL;
		}

		productURL = productURL.substring(0, productURL.indexOf('?'));
		for (String paramEntry: urlParam.split("&")) {
			String paramName = paramEntry.substring(0, (paramEntry.indexOf("=") > 0 ? paramEntry.indexOf("=") : 0));
			String paramValue = (paramEntry.indexOf("=") > 0 ? paramEntry.substring(paramEntry.indexOf("=") + 1) : "");
			if (paramName.startsWith("utm_")) continue;

			boolean found = false;
			for (String skipParamName: arrSkipParameters) {
				if (skipParamName.trim().toLowerCase().equals(paramName.trim().toLowerCase())) {
					found = true;
					break;
				}
			}

			if (!found) {
				if (sb.length() > 0) sb.append('&');
				sb.append(paramName);
				if (paramValue != '') sb.append('=').append(paramValue);
			}
		}

		if (productURL.indexOf('?') > -1 && !productURL.endsWith('&')) productURL += '&';
		else if (productURL.indexOf('?') == -1) productURL += '?';
		productURL += sb.toString();

		if (addDel) {
			if (productURL.indexOf('?') > -1 && (!productURL.endsWith('&') && !productURL.endsWith('?'))) productURL += '&';
			else if (productURL.indexOf('?') == -1) productURL += '?';
		}

		return productURL;
	}


	private static String removeAcentos(String string) {
		string = string.replaceAll("[ÂÀÁÄÃ]", "A");
		string = string.replaceAll("[âãàáä]", "a");
		string = string.replaceAll("[ÊÈÉË]", "E");
		string = string.replaceAll("[êèéë]", "e");
		string = string.replaceAll("ÎÍÌÏ", "I");
		string = string.replaceAll("îíìï", "i");
		string = string.replaceAll("[ÔÕÒÓÖ]", "O");
		string = string.replaceAll("[ôõòóö]", "o");
		string = string.replaceAll("[ÛÙÚÜ]", "U");
		string = string.replaceAll("[ûúùü]", "u");
		string = string.replaceAll("Ç", "C");
		string = string.replaceAll("ç", "c");
		string = string.replaceAll("[ýÿ]", "y");
		string = string.replaceAll("Ý", "Y");
		string = string.replaceAll("ñ", "n");
		string = string.replaceAll("Ñ", "N");
		return string;
	}

	private static String getStringForTracking(String conteudo) {
		if (conteudo == null) return null;
		conteudo = removeAcentos(conteudo);
		conteudo = Normalizer.normalize(conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]", "-").toLowerCase();
	}

}