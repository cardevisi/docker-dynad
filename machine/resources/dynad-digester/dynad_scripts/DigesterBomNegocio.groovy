import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;

import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterBomNegocio {
		static java.util.List<String> listaCategorias = new java.util.ArrayList(), listaRegioes = new java.util.ArrayList();
		static { 
			listaCategorias.add('1000'); listaCategorias.add('2000'); listaCategorias.add('3000'); listaCategorias.add('5000');
			listaCategorias.add('6000'); listaCategorias.add('7000'); listaCategorias.add('8000'); listaCategorias.add('9000');
			listaCategorias.add('10000'); listaCategorias.add('11000'); listaCategorias.add('1020'); listaCategorias.add('1040');
			listaCategorias.add('1060'); listaCategorias.add('1080'); listaCategorias.add('1100'); listaCategorias.add('1120');
			listaCategorias.add('2020'); listaCategorias.add('2040'); listaCategorias.add('2060'); listaCategorias.add('2080');
			listaCategorias.add('2100'); listaCategorias.add('5020'); listaCategorias.add('5040'); listaCategorias.add('5060');
			listaCategorias.add('5080'); listaCategorias.add('5100'); listaCategorias.add('6020'); listaCategorias.add('6060');
			listaCategorias.add('11020'); listaCategorias.add('11040'); listaCategorias.add('11060'); listaCategorias.add('11080');
			listaCategorias.add('11100'); listaCategorias.add('3020'); listaCategorias.add('3040'); listaCategorias.add('3060');
			listaCategorias.add('3080'); listaCategorias.add('7020'); listaCategorias.add('7040'); listaCategorias.add('7060');
			listaCategorias.add('7080'); listaCategorias.add('8020'); listaCategorias.add('8040'); listaCategorias.add('8060');
			listaCategorias.add('8080'); listaCategorias.add('9020'); listaCategorias.add('10020');
			
			listaRegioes.add('68'); listaRegioes.add('82'); listaRegioes.add('92'); listaRegioes.add('97'); listaRegioes.add('96');
			listaRegioes.add('71'); listaRegioes.add('73'); listaRegioes.add('74'); listaRegioes.add('75'); listaRegioes.add('77');
			listaRegioes.add('85'); listaRegioes.add('88'); listaRegioes.add('61'); listaRegioes.add('27'); listaRegioes.add('28');
			listaRegioes.add('62'); listaRegioes.add('64'); listaRegioes.add('98'); listaRegioes.add('99'); listaRegioes.add('31');
			listaRegioes.add('32'); listaRegioes.add('33'); listaRegioes.add('34'); listaRegioes.add('35'); listaRegioes.add('37');
			listaRegioes.add('38'); listaRegioes.add('67'); listaRegioes.add('65'); listaRegioes.add('66'); listaRegioes.add('91');
			listaRegioes.add('93'); listaRegioes.add('94'); listaRegioes.add('83'); listaRegioes.add('81'); listaRegioes.add('87');
			listaRegioes.add('86'); listaRegioes.add('89'); listaRegioes.add('41'); listaRegioes.add('42'); listaRegioes.add('43');
			listaRegioes.add('44'); listaRegioes.add('45'); listaRegioes.add('46'); listaRegioes.add('21'); listaRegioes.add('22');
			listaRegioes.add('24'); listaRegioes.add('84'); listaRegioes.add('69'); listaRegioes.add('95'); listaRegioes.add('51');
			listaRegioes.add('53'); listaRegioes.add('54'); listaRegioes.add('55'); listaRegioes.add('47'); listaRegioes.add('48');
			listaRegioes.add('49'); listaRegioes.add('79'); listaRegioes.add('11'); listaRegioes.add('12'); listaRegioes.add('13');
			listaRegioes.add('14'); listaRegioes.add('15'); listaRegioes.add('16'); listaRegioes.add('17'); listaRegioes.add('18');
			listaRegioes.add('19');
			
		}
        static int key;
        static {
            ConnHelper.schema = "dynad_bomnegocio";
            Connection conn = ConnHelper.get().reopen();
            PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
            ResultSet res = pStmt.executeQuery();
            if( res.next() )
                    key = res.getInt(1);
            ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;


		public static String unaccent(String s) {
		    String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
		    return normalized.replaceAll("[^\\p{ASCII}]", "");
		}

        private static markXml ( String file, String c, String r ) {

	        Connection conn = ConnHelper.get().reopen();
	
			int cline = 0;
			def p = null;
			String line = null;
			
			String sku, c1, c2, c3, c4, c5, categoria;
			String image, oprice, fprice, nparcelas, vparcelas;
			String url, marca, nome, codigo, promover, desconto;
			String dataInicio, dataFim, numDiarias, numPessoas, estado, regiao, pais, cidade, cta;
			
			java.io.File f = new java.io.File( file );
			if( !f.exists() ) return;
			java.io.FileInputStream ins = new java.io.FileInputStream(f); 
			java.io.InputStreamReader isr = new java.io.InputStreamReader(ins, 'ISO-8859-1'); 
			char [] buffer = new char[ f.size() ];			
			int read = isr.read( buffer );
			String s = String.valueOf(buffer, 0, read);			
			isr.close();
	
			StringBuilder sb = new StringBuilder();
			String opening = "<description><![CDATA[", ending = "]]></description>";
			int p1 = -1, p2 = -1;	
			while( true ) { 
				if( (p1 = s.indexOf( opening ) ) > -1 ) {
					if( (p2 = s.indexOf( ending ) ) > -1 ) {
						if( p2 > p1 ) { 
							sb.append( s.substring(0, p1) );
							s = s.substring( p2 + ending.length() );
						}
					} else
						break;					
				} else
					break;
			}			
			sb.append(s);		
			def summary = new XmlParser().parseText(sb.toString());
	
	        //def summary = new XmlParser().parse(file);
	        def list = summary.product
			/**
	<product id="37814996">
		<name><![CDATA[Towner de passageiro]]></name>
		<producturl><![CDATA[http://sp.bomnegocio.com/sao-paulo-e-regiao/veiculos/carros/towner-de-passageiro-37814996]]></producturl>
		<bigimage><![CDATA[http://img.bomnegocio.com/images/41/4125352606.jpg]]></bigimage>
		<categoryid1><![CDATA[Veículos e barcos]]></categoryid1>
		<categoryid2><![CDATA[Carros]]></categoryid2>
		<description><![CDATA[Vendo towner ano 95 de passageiro com pequeno débito, valor 5.000,00 aceito carro na troca, mecanica cambio e parte eletrica em bom estado, contatos por tel, e-mail ou whatsapp]]></description>
		<instock><![CDATA[true]]></instock>
	</product>
			 */
	        list.each {
	        	sku = it.@id;
	        	nome = it.name.text().trim();
	        	url = it.producturl.text();
	            oprice = (it.price != null && it.price.text() != null && it.price.text() != '' ?  it.price.text() : '0.00');
				nparcelas = '1';
				vparcelas = oprice;
	            fprice = oprice;
	            desconto = '0';
	        	image = it.bigimage.text()
				marca = null;
				
				c1 = r;
				c2 = c;
				c3 = it.categoryid1.text().trim();
				c4 = it.categoryid2.text().trim();
				
				categoria = c1;
				if( c2 != null ) categoria += '/' + c2;
				if( c3 != null ) categoria += '/' + c3;
				if( c4 != null ) categoria += '/' + c4;
				
				
				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: (image == null || image == '' ? '0' : '1' ), lookupChange: false, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria4', columnType: 'string', columnValue: c4, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );

	            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
			}
		
	}

	public static void complementaRecomendacoes ( Connection conn ) { 
    	PreparedStatement pStmt = null, pStmt2 = null;
    	ResultSet res = null, res2 = null; 
    	
    	java.util.List<String> lstBestSellersCategoriaGeral = new java.util.ArrayList();
		pStmt2 = conn.prepareStatement("SELECT sku from catalogo where ativo = '1' LIMIT 0, 11");
    	res2 = pStmt2.executeQuery();
		while( res2.next() ) { 
			lstBestSellersCategoriaGeral.add( res2.getString(1) );
		}
		ConnHelper.closeResources(pStmt2, res2);
		
    	
    	pStmt = conn.prepareStatement("SELECT id, categoria from codigos_categorias");
    	res = pStmt.executeQuery();
    	while( res.next() ) { 
    		java.util.List<String> lstBestSellersCategoria = new java.util.ArrayList();
    		pStmt2 = conn.prepareStatement("SELECT sku from catalogo where id_categoria = ? AND ativo = '1' LIMIT 0, 11");
    		pStmt2.setInt(1 , res.getInt(1));
        	res2 = pStmt2.executeQuery();
    		while( res2.next() ) { 
    			lstBestSellersCategoria.add( res2.getString(1) );
    		}
    		ConnHelper.closeResources(pStmt2, res2);
    		
    		if( lstBestSellersCategoria.size() < 11 ) {
    			lstBestSellersCategoria.clear();
    			lstBestSellersCategoria.addAll( lstBestSellersCategoriaGeral );
    		}
    		
			StringBuilder sb = new StringBuilder();
			for(String sku : lstBestSellersCategoria ) { 
				sb.append(",[").append( sku ).append("]");
			}
			sb.append(",");
			
			String stmt = "update catalogo set recomendacoes =  replace( replace( replace( replace( replace(?, concat( concat('[', sku), ']'), '[0]') , ',[0],', ','), '],[', ','), ',[', ''), '],', '') where recomendacoes is null and id_categoria = ?";
			pStmt2 = conn.prepareStatement(stmt);
			pStmt2.setString(1, sb.toString());
			pStmt2.setInt(2, res.getInt(1));
			pStmt2.executeUpdate();
    	}
    	ConnHelper.closeResources(pStmt, res);
    }

	public static void limpaRecomendacoes ( Connection conn ) {
    		PreparedStatement pStmt = null, pStmt2 = null;
	    	ResultSet res = null, res2 = null;
		pStmt2 = conn.prepareStatement( "update catalogo set recomendacoes = null" );
		pStmt2.executeUpdate();
    	}

	public static void normalizaDB ( Connection conn ) { 
    	PreparedStatement pStmt = null, pStmt2 = null;
    	ResultSet res = null, res2 = null; 
    	
    	long idCategoria = 1l;
		java.util.Map<String, Long> mapeamentoCategorias = new java.util.HashMap();
		pStmt2 = conn.prepareStatement("SELECT categoria, id FROM codigos_categorias order by id");
    	res2 = pStmt2.executeQuery();
		while( res2.next() ) {
			mapeamentoCategorias.put( res2.getString(1), res2.getLong(2) );
			idCategoria = res2.getLong(2) + 1;
		}
		ConnHelper.closeResources(pStmt2, res2);
    	
		pStmt2 = conn.prepareStatement("SELECT DISTINCT categoria, categoria1, categoria2, categoria3,categoria4 from catalogo where id_categoria is null");
    	res2 = pStmt2.executeQuery();
		while( res2.next() ) {
			long id = 0l;
			if( !mapeamentoCategorias.containsKey(res2.getString(1)) ) {
				idCategoria++;
				id = idCategoria;
			println('***** NOVA CATEGORIA: ' + res2.getString(1) );
			pStmt = conn.prepareStatement("INSERT INTO codigos_categorias (id, categoria, categoria1, categoria2, categoria3, categoria4) values (?, ?, ?, ?, ?, ?)");
			pStmt.setLong(1, id);
			pStmt.setString(2, res2.getString(1));
			pStmt.setString(3, res2.getString(2));
			pStmt.setString(4, res2.getString(3));
			pStmt.setString(5, res2.getString(4));
			pStmt.setString(6, res2.getString(5));
			pStmt.executeUpdate();
			} else
				id = mapeamentoCategorias.get(res2.getString(1));
			
			pStmt = conn.prepareStatement("UPDATE catalogo SET id_categoria = ? WHERE id_categoria is null AND categoria = ?");
			pStmt.setLong(1, id);
			pStmt.setString(2, res2.getString(1));
			pStmt.executeUpdate();
		}
		ConnHelper.closeResources(pStmt2, res2);
    }
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			java.io.File dirPendentes = new java.io.File("/mnt/XML/bomnegocio/pendentes");
			for( java.io.File f : dirPendentes.listFiles() ) { 
				String [] dados = f.getName().substring(0, f.getName().indexOf('.')).split("_");
				String cat = dados[1], reg = dados[2];
				try {
					println('file: ' + f.getName() + ' / cat: ' + cat + ' / reg: ' + reg );
                    			markXml(f.getAbsolutePath() , cat, reg );
					f.delete();
		                } catch ( org.xml.sax.SAXParseException exx ) {
                		}
			}
		normalizaDB( ConnHelper.get().reopen() );
        	//DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
		//limpaRecomendacoes ( ConnHelper.get().reopen() );
        	//DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
        	//DigesterV2.completaRecomendacoes(ConnHelper.get().reopen(), false);
		complementaRecomendacoes(ConnHelper.get().reopen());
        	//DigesterV2.bestSellers(ConnHelper.get().reopen());
        	//DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		} else
		if(args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen());
			DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		}
	}
	
}

