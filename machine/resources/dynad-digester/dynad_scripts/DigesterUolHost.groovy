import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import java.text.Normalizer

@Grapes([
  	@Grab(group='org.apache.poi', module='poi', version='3.7'),
  	@Grab(group='commons-codec', module='commons-codec', version='1.6'),
  	@Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  	@Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  	@GrabConfig(systemClassLoader=true)
])

class DigesterUolHost {
    static int key;
    static {
        ConnHelper.schema = "dynad_uolhost";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
                key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }

    public static boolean _DEBUG_ = false;

    private static markFeed ( String file ) {		
        Connection conn = ConnHelper.get().reopen();
		
		int cline = 0;
		def p = null;
		String line = null;

		String sku, nome, descricao, url, image, price, c1, c2, c3, promocao, topSeller;		

		FileInputStream fileInputStream = new FileInputStream(file);
		HSSFWorkbook excel = new HSSFWorkbook(fileInputStream);
		HSSFSheet folha = excel.getSheetAt(0);	
		Iterator linhas = folha.iterator();

		if (linhas.hasNext()) linhas.next();

	 	while (linhas.hasNext()) {
	 		HSSFRow linha = linhas.next();	 			 		
	 		DataFormatter formatter = new DataFormatter(); //formater
		 	sku = formatter.formatCellValue(linha.getCell(0)); 
		 	nome = formatter.formatCellValue(linha.getCell(1)); 
		 	descricao = formatter.formatCellValue(linha.getCell(3)); 
		 	url = formatter.formatCellValue(linha.getCell(2)); 
		 	if (url.trim() == '-') url = null;
		 	image = formatter.formatCellValue(linha.getCell(12));
		 	if (image.trim() == '-') image = null;
		 	price = formatter.formatCellValue(linha.getCell(4));
		 	price = Digester.autoNormalizaMoeda(price, false, new Locale("pt", "BR"));
		 	c1 = formatter.formatCellValue(linha.getCell(8));
		 	c2 = formatter.formatCellValue(linha.getCell(9));
		 	c3 = formatter.formatCellValue(linha.getCell(10));
		 	promocao = formatter.formatCellValue(linha.getCell(5));
		 	topSeller = formatter.formatCellValue(linha.getCell(6));	 			 		

		 	FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: topSeller, lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1 + '/' + c2, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: price, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: price, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: price, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'promocao', columnType: 'string', columnValue: promocao, lookupChange: false, platformType: 'NA') );

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 ) conn = ConnHelper.get().reopen();
	 	}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markFeed( '/mnt/TMP/xml/uolhost.xls' );
        	DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
        	DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
        	DigesterV2.completaRecomendacoes(ConnHelper.get().reopen(), false, 5, RecommenderMode.DIFFERENT_NAME);
        	//DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
        	//DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		} else
		if(args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
			//DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		}		

		///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("SELECT sku, id, imagem, ativo, nome, link, preco_promocional, descricao, promocao, categoria, recomendacoes FROM catalogo WHERE sku IS NOT NULL AND id IS NOT NULL AND ativo IS NOT NULL AND categoria IS NOT NULL");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/uolhost.txt").withWriter { out ->
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                if (res.getString(3)) out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                else out.print("|");
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+ (res.getString(i) ? res.getString(i).replace("|", " ") : "" ));
                out.print("\n");                
                if (res.getString(9)) out.print(res.getString(10)+"|"+res.getString(1)+"\n");//escrevo a categoria com promoção
            }
        }
        ConnHelper.closeResources(pStmt, res);
        println "SNA data generated";
	}	
}