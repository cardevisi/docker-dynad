echo "INICIO - Americanas"

date
START=$(date +%s)

### PARAMETROS ###
XML_FILE=/mnt/XML/americanas.xml
TEMP_NAME=americanas
SCHEMA=dynad_americanas
DB_HOST=74.81.70.46
EXP_HOST="68.233.252.114 23.23.92.125 74.81.70.46 54.232.127.115"
DIGESTER=/root/dynad_scripts/DigesterAmericanas.groovy
##################



RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old.2
mv $XML_FILE.old.1 $XML_FILE.old.2
rm $XML_FILE.old.1
mv $XML_FILE.old $XML_FILE.old.1
rm $XML_FILE.old
mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."

export SSHPASS='0Wy1>aJtu'
sshpass -e sftp -oBatchMode=no -b - xml.lomadee.acom@sftp.americanas.com.br << !
   lcd /tmp
   get XML_ACOM_LOMADEE-1.zip
   get XML_ACOM_LOMADEE-2.zip
   get XML_ACOM_LOMADEE-3.zip
   get XML_ACOM_LOMADEE-4.zip
   get XML_ACOM_LOMADEE-5.zip
   get XML_ACOM_LOMADEE_LIVROS-1.zip
   get XML_ACOM_LOMADEE_LIVROS-2.zip
   get XML_ACOM_LOMADEE_LIVROS-3.zip
   bye
!


cd /tmp
unzip /tmp/XML_ACOM_LOMADEE-1.zip
unzip /tmp/XML_ACOM_LOMADEE-2.zip
unzip /tmp/XML_ACOM_LOMADEE-3.zip
unzip /tmp/XML_ACOM_LOMADEE-4.zip
unzip /tmp/XML_ACOM_LOMADEE-5.zip
unzip /tmp/XML_ACOM_LOMADEE_LIVROS-1.zip
unzip /tmp/XML_ACOM_LOMADEE_LIVROS-2.zip
unzip /tmp/XML_ACOM_LOMADEE_LIVROS-3.zip
#unzip /tmp/XML_ACOM_LOMADEE_LIVROS-4.zip
cd /root/dynad_scripts

a=`wc -l /tmp/XML_ACOM_LOMADEE-1.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE-1.xml | head -n `expr $a - 2` | sed 's/&/e/g' > $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE-2.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE-2.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE-3.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE-3.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE-4.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE-4.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE-5.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE-5.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE_LIVROS-1.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE_LIVROS-1.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE_LIVROS-2.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE_LIVROS-2.xml | tail -n `expr $a - 6` | head -n `expr $a - 8` | sed 's/&/e/g' >> $XML_FILE
a=`wc -l /tmp/XML_ACOM_LOMADEE_LIVROS-3.xml | awk '{print $1}'`; cat /tmp/XML_ACOM_LOMADEE_LIVROS-3.xml | tail -n `expr $a - 6` | sed 's/&/e/g' >> $XML_FILE

#curl $XML_URL |  iconv -f iso8859-1 -t utf-8 > $XML_FILE

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi




cmp -s $XML_FILE $XML_FILE.old >/dev/null 
if [ $? -ne 0 ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

#  echo "executando script de normalizacao ..."
#  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql
#
#  echo "fazendo export do banco ..."
#  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE
#
#  for ip in $EXP_HOST; do
#     echo "exportando para $ip"
#     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
#  done
#
else

  if [ $RANKING -eq 1 ]
  then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

#    echo "fazendo export do banco (best_sellers) ..."
#    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE
#
#    for ip in $EXP_HOST; do
#       echo "exportando para $ip"
#       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
#    done
#
  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"


