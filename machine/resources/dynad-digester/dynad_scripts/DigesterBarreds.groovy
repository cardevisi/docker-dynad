import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterBarreds {
	static int key;
	static {
		ConnHelper.schema = "dynad_barreds";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;

        def rss = new XmlParser().parse(file);
        def list = rss.produto
        
		list.each {
			sku = it.id_produto.text()
			nome = Digester.stripTags(Digester.ISO2UTF8(it.descricao.text())) 
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");

            url = it.link_produto.text()
//			if(url.indexOf("?") > -1)
//				url = url.substring(0, url.indexOf("?"));

			oprice = it.precode.text()
			if(oprice.indexOf(",") > -1) {
				int tam = oprice.indexOf(",") + 3;
				if(tam > oprice.length()) tam = oprice.length();

				oprice = oprice.substring(0, tam);
			}
			oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"));
			
			fprice = it.preco_normal.text()
			if(fprice.indexOf(",") > -1) {
				int tam = fprice.indexOf(",") + 3;
				if(tam > fprice.length()) tam = fprice.length();

				fprice = fprice.substring(0, tam);
			}
			fprice = Digester.autoNormalizaMoeda(fprice, false, new Locale("pt", "BR"));

			nparcelas = it.parcelamento.num_parcelas.text();                    
			vparcelas = it.parcelamento.valor_parcela.text()
			if(vparcelas.indexOf(",") > -1) {
				int tam = vparcelas.indexOf(",") + 3;
				if(tam > vparcelas.length()) tam = vparcelas.length();

				vparcelas = vparcelas.substring(0, tam);
			}
			
			String textoParcelamento = it.parcelamento.text();
			if( textoParcelamento.indexOf('vista') > -1 ) { 
				nparcelas = '1';
				vparcelas = fprice;
			} else { 
				textoParcelamento = textoParcelamento.substring( textoParcelamento.indexOf(" ") + 1 );
				nparcelas = textoParcelamento.substring(0, textoParcelamento.indexOf("x"));
				textoParcelamento = textoParcelamento.substring( textoParcelamento.indexOf("de ") + 3 );
				vparcelas = textoParcelamento.substring(0, textoParcelamento.indexOf(" "));
			}
				
            vparcelas = Digester.autoNormalizaMoeda(vparcelas, false, new Locale("pt", "BR"));
			image = it.imagem.text();

         	c1 = it.categoria.text()
         	c2 = "";
         	c3 = ""

			fprice = fprice==null||fprice==""?"0,00":fprice;
			oprice = oprice==null||oprice==""?"0,00":oprice;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;
                
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/barreds.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
            DigesterV2.validacao(ConnHelper.get().reopen(), 140, 15.0);
        } else
	        if(args[0] == 'BEST_SELLERS') {
                Digester.bestSellers(ConnHelper.get().reopen());
	        }
    }
}

