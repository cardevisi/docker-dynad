import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;


class Main {
	static java.util.List<String> urls = new java.util.ArrayList<String>(); 
	static index = -1;
	
	static java.io.BufferedWriter out = null;
	
	static java.util.List<String> paginacao ( TagNode rootNode ) { 
		java.util.List<String> paginacao = new java.util.ArrayList<String>();
		TagNode [] divElements = rootNode.getElementsByName("ul", true);
		for (int i = 0; divElements != null && i < divElements.length; i++)
		{
			TagNode [] liElements = divElements[i].getElementsByName("li", true);
			for (int x = 0; liElements != null && x < liElements.length; x++)
			{
	
				String classType = liElements[x].getAttributeByName("class");
				if ( 'catalog_paging_page'.equals(classType) )
				{
					TagNode [] aElements = liElements[x].getElementsByName("a", true);
					if( aElements.length > 0 && !'current'.equals(  aElements[0].getAttributeByName('class') ) )
						paginacao.add( 'http://www.dafiti.com.br' +  aElements[0].attributes['href'] ) ;
					
				}
			}
			
		}
		
		return paginacao;
	}
	
	static void catalogo ( TagNode rootNode ) { 
		StringBuilder sb = new StringBuilder();
		TagNode [] divElements = rootNode.getElementsByName("div", true);
		for (int i = 0; divElements != null && i < divElements.length; i++)
		{
			if( 'breadcrumb'.equals( divElements[i].getAttributeByName("id") ) ) {
				TagNode [] a = divElements[i].getElementsByName("a", true);
				a.each { TagNode it ->
					sb.append('/').append( it.text.toString() );
				}
			}
		}
		
		divElements = rootNode.getElementsByName("ul", true);
		for (int i = 0; divElements != null && i < divElements.length; i++)
		{
			if( 'catalog_grid'.equals( divElements[i].getAttributeByName("class") ) ) { 
				
				TagNode [] liElements = divElements[i].getElementsByName("li", true);
				for (int x = 0; liElements != null && x < liElements.length; x++)
				{
					produto( liElements[ x ], sb.toString() );
				}
				
			}
			
		}
	}
	
	static void produto ( TagNode nodeProduto, String breadcrumb ) { 
		TagNode [] liElements = nodeProduto.childTags;
		
		String brand = '';
		String tituloProduto = null, linkProduto = null, rate = null, frete = null, imagem = null;
		for (int x = 0; liElements != null && x < liElements.length; x++) { 
			TagNode [] divElements = liElements[x].childTags;
			for (int i = 0; divElements != null && i < divElements.length; i++)
			{
				if( divElements[i].getAttributeByName("class") != null && 
					divElements[i].getAttributeByName("class").indexOf( 'catalog_frete_gratis' ) > -1 ) 
					frete = divElements[i].text.toString();
					
				if( 'catalog-rates'.equals( divElements[i].getAttributeByName("class") ) ) 
					rate = divElements[i].text.toString();
					
				if( 'image'.equals( divElements[i].getAttributeByName("class") ) )
					imagem = divElements[i].attributes['id'];
					
				if( 'catalog_brand'.equals( divElements[i].getAttributeByName("class") ) ) 
					brand = divElements[i].text.toString();
					
				if( 'catalog-product-name'.equals( divElements[i].getAttributeByName("class") ) ) { 
					TagNode a = divElements[i].getElementsByName("a", true)[0];
					tituloProduto = a.attributes['title'];
					linkProduto = a.attributes['href'];
				}
					
			}
			
		}
		
		println('--------------------------');
		println('brand: ' + brand);
		println('tituloProduto: ' + tituloProduto);
		println('linkProduto: ' + linkProduto);
		println('imagemProduto: ' + imagem);
		println('rate: ' + rate?.trim());
		println('frete: ' + frete);
		
		StringBuilder sb = new StringBuilder();
		sb.append('"').append( brand ).append('";"').
			append( breadcrumb ).append('";"').
			append( tituloProduto ).append('";"').
			append( linkProduto ).append('";"').
			append( imagem ).append('";"').
			append( rate?.trim() ).append('";"').
			append( frete ).append('"').append("\r\n");
			
		out.write( sb.toString().getBytes() );	
	} 
	
	static String getFirstTagValue ( TagNode node, String name ) { 
		TagNode [] divElements = node.getElementsByName(name, true);
		for (int i = 0; divElements != null && i < divElements.length; i++)
		{
			return divElements[i].text.toString();
		}
		return null;
	}
	
	static TagNode getFirstTag ( TagNode node, String name ) {
		TagNode [] divElements = node.getElementsByName(name, true);
		for (int i = 0; divElements != null && i < divElements.length; i++)
		{
			return divElements[i];
		}
		return null;
	}
	
	static boolean hasElement ( TagNode node, String name ) {
		TagNode [] divElements = node.getElementsByName(name, true);
		return divElements.length > 0; 
	}
	
	static void processar ( ) { 
		if( ++index >= urls.size() )
			return;
			
		TagNode rootNode;
		HtmlCleaner cleaner = new HtmlCleaner();
		rootNode = cleaner.clean(new URL( urls.get(index) ) );
		
		println('processando pagina: ' + urls.get(index) );
		catalogo( rootNode );
		
		paginacao( rootNode ).each {
			if( urls.indexOf( it ) == -1 ) urls.add( it );
		}
		
		processar ( );
	}
	
	static java.util.List<ShortProduct> recomendacoes (String url ) { 
		/**try { 
			Thread.sleep( 2000 );
		} catch ( Exception ex) {}**/
		
		java.util.List<String> skus = new java.util.ArrayList<String>();
		
		StringBuilder sb = null;
		while( true ) {
			try {
				sb = new StringBuilder();
				url.toURL().withReader { reader ->
					sb.append( reader.readLine() );
				}
				break;
			} catch ( java.net.SocketTimeoutException ex) { }
		}

		int posi = -1;			
		while( (posi = sb.indexOf('sku=') ) > -1 ) { 
			sb.delete(0, posi + 4);
			if( skus.indexOf( sb.substring(0, sb.indexOf('&') ) ) == -1 )
				skus.add ( sb.substring(0, sb.indexOf('&') ) );
			sb.delete(0, sb.indexOf('&') + 1);
		}
		
		java.util.List<ShortProduct> product = new java.util.ArrayList<ShortProduct>();
		int maxIndex = 0;
		for(String sku : skus) { 
			product.add ( detalhes( sku ) );
			if( ++maxIndex == 3 ) break;
		}
			
		return product;
	}
	
	static ShortProduct detalhes ( String sku ) { 
		String url = 'http://www.dafiti.com.br/catalog/recommendationview/?sku=' + sku + '&tracking=recommendations&pageName=product&position=3&isVertical=1';
		StringBuilder sb = null;
		while( true ) { 
			try { 
				sb = new StringBuilder();
				url.toURL().eachLine { line ->
					sb.append( new String( line.getBytes(), 'UTF-8') );
				}
				break;
			} catch ( java.net.SocketTimeoutException ex) { }
		}
		
		TagNode rootNode;
		HtmlCleaner cleaner = new HtmlCleaner();
		rootNode = cleaner.clean( sb.toString() );
		
		
		
		String imagem = null, brand = null, productName = null, productUrl = null, price = null, rates = null, specialPrice = null;
		TagNode [] divElements = rootNode.getElementsByAttValue('class', 'image', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) imagem = divElements[i].getAttributeByName('id');
		
		divElements = rootNode.getElementsByAttValue('class', 'catalog_brand', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) brand = divElements[i].text.toString();
		divElements = rootNode.getElementsByAttValue('class', 'product-image', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) {
			productName = divElements[i].getAttributeByName('title');
			productUrl = 'http://www.dafiti.com.br' + divElements[i].getAttributeByName('href');
		}
		
		divElements = rootNode.getElementsByAttValue('class', 'price', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) price = divElements[i].text.toString();
		
		divElements = rootNode.getElementsByAttValue('class', 'specialprice', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) specialPrice = divElements[i].text.toString();

		divElements = rootNode.getElementsByAttValue('class', 'catalog-rates', true, true);
		for (int i = 0; divElements != null && i < divElements.length; i++) rates = divElements[i].text.toString().trim();

		if( specialPrice == null ) specialPrice = price;
		
		return new ShortProduct( 
			id : sku,
			image : imagem,
			marcas : brand,
			name : productName,
			url : productUrl,
			price: price,
			rate : rates,
			specialPrice : specialPrice
		);	
	}
	
	public static void main ( String [] args ) {
		out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream( new java.io.File('rec.out'), true ), 'UTF-8' ) );
		int cLine = 0;
		new File( 'out2.csv' ).eachLine() { line ->
			if( ++cLine <= 429 ) return; 
			
			String [] data = line.trim().split(';');
			String url = data[4].replaceAll('"', '').replaceAll('"', '');
			
			String codigo = url.tokenize("/")[-1];
			if( codigo.indexOf('?') > -1 )
				codigo = codigo.substring(0, codigo.indexOf('?'));
			codigo = codigo.tokenize("-")[-1];
			
			if( codigo.indexOf('.') > -1 )
				codigo = codigo.substring(0, codigo.indexOf('.'));
				
			println( 'processando codigo: ' + codigo );
			int index = 0;			
			recomendacoes( 'http://www.dafiti.com.br/recommendation/recommendation/?c=catalog&a=detail&category=0&brand=0&color=0&price=0&upper_material=0&sole_material=0&inner_material=0&heel_shape=0&heel_height=0&trodden_type=0&gender=0&prod=' + codigo + '&new_products=0&special-price=0' ).each { product ->
				if( index++ > 0 ) {
					print(';');
					out.write( ';' );
				} else { 
					print( line.trim()  );
					out.write( line.trim() );
				}	 
				print( product.get() );
				out.write( product.get() );
			};
			println('');
			out.write( "\r\n" );
			out.flush();
		}
		
		try { out.close(); } catch ( Exception ex) {}
		//detalhes( 'http://www.dafiti.com.br/recommendation/recommendation/?c=catalog&a=detail&category=0&brand=0&color=0&price=0&upper_material=0&sole_material=0&inner_material=0&heel_shape=0&heel_height=0&trodden_type=0&gender=0&prod=425&new_products=0&special-price=0&YII_CSRF_TOKEN=5521876a7e832bc24b8d1cb5bae3b7e1ce81be66&page=1&callback=jQuery1707838381667963461_1333291540153&product_amount=3&_=1333291541117' );
		/**
		try { 
			
			out = new FileOutputStream( new java.io.File('records.out'), true );
			
			index = -1;
			urls.add( 'http://www.dafiti.com.br/calcados-femininos/new-products/' ) 
			processar ( );
			
			println('termino');
		} finally { 
			if( out != null )
				try { out.close(); } catch ( Exception ex) {}
		}
		**/	
	}
	

}

class ShortProduct {
	String id;
	String name;
	String image;
	String price;
	String rate;
	String url;
	String marcas;
	String specialPrice;
	String nParcelas;
	String vParcelas;
	String get () {
		
		return '"' + id + '";"' +
			name + '";"' +
			image + '";"' +
			url + '";"' +
			price + '";"' +
			rate + '";"' +
			nParcelas + '";"' +
			vParcelas + '";"' +
			specialPrice + '";"' +
			marcas + '"';
		
	}
	
}
