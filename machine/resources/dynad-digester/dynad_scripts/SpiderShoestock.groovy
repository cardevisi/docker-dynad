import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @Grab('org.jsoup:jsoup:1.6.1'),
  @GrabConfig(systemClassLoader=true)
])


class Shoestock {

	static int key;

        public static boolean _DEBUG_ = false;
        public static String toUTF8(s) {return new String(((String)s).getBytes("UTF-8")); }
        
	static {
		ConnHelper.schema = "dynad_shoestock";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
	}
	
	public static String [] extractInformation(String pg) {
		Document doc
		String [] ret = new String[2];
		
		if(_DEBUG_) print("  " + pg + "...");
		
        try{
			doc = Jsoup.connect(pg).timeout(120000).userAgent("Mozilla").get();
		}catch(ex){
        	if(_DEBUG_) println("fail");
          	return null;
        }
		 
        try{
			ret[0] = doc.select(".notifyme-skuid").first().attr("value");
			ret[1] = doc.select(".productDescription").first().text();
		}catch(Exception ex) { 
			try{
				ret[0] = doc.select(".calculoFrete").first().attr("skuCorrente");
				ret[1] = doc.select(".productDescription").first().text(); 
			}catch(Exception exx){
				exx.printStackTrace(); println("FALHOU AO IDENTIFICAR PRODUTO !!!");
				return null;
			}
		}
		return ret;
	}

	public static String extractSKU(String pg) {
		Document doc
		
		if(_DEBUG_) print("  " + pg + "...");
		
                try {
			doc = Jsoup.connect(pg).timeout(120000).userAgent("Mozilla").get();
                } catch(ex) {
                	if(_DEBUG_) println("fail");
                	return;
                }
                
		def ret = ""; 
                try { ret = doc.select(".notifyme-skuid").first().attr("value");} catch(Exception ex) { 
			try{ doc.select(".calculoFrete").first().attr("skuCorrente"); } catch(Exception exx) {
				exx.printStackTrace(); println("FALHOU AO IDENTIFICAR PRODUTO !!!");
			}
		}
		return ret;
	}
        
        public static loadPage(String pg, String c1, String c2, String c3) {
		
		Document doc
		
		if(_DEBUG_) print("  " + pg + "...");
		
                try {
			doc = Jsoup.connect(pg).timeout(120000).userAgent("Mozilla").get();
                } catch(ex) {
                	if(_DEBUG_) println("fail");
                	return;
                }
                
                Boolean foi = false;
                Connection conn = ConnHelper.get().reopen();
                def cline = 0;
                
                doc.select(".productContent").each { prod->
                	def img = prod.select(".productImage").first()
                	def img_a = img.select("a").first()
                	def nome = Shoestock.toUTF8(img_a.attr("title"));
					def url = img_a.attr("href");
                	//def sku = prod.select(".productId").first().text();

                	String [] extract = Shoestock.extractInformation(url);
					if(extract == null){
						return null;
					}
					def sku = extract[0];					
					def comentario = extract[1];
                	//def sku = Shoestock.extractSKU(url);

                	if( comentario != null && comentario.length() > 2048 )
                		comentario = comentario.substring(0, 2045) + "...";	

                	def image = img.select("img")first().attr("src");

					def oprice = "";
					def fprice = "";
					def nparcelas = "";
					def vparcelas = "";
					def ativo = 'sim';
                	prod.select(".productInfoWrapper").each { a->
					//oprice = Digester.normalizaMoeda(a.select(".vtex-cpListPrice").first().text().replaceAll(/R\$/, "").trim(), true);
					oprice = Digester.autoNormalizaMoeda(a.select(".vtex-cpListPrice").first().text().replaceAll(/R\$/, "").trim(), true, new Locale("en", "US"));
					//fprice = Digester.normalizaMoeda(a.select(".vtex-cpBestPrice").first().text().replaceAll(/R\$/, "").trim(), true);
					fprice = Digester.autoNormalizaMoeda(a.select(".vtex-cpBestPrice").first().text().replaceAll(/R\$/, "").trim(), true, new Locale("en", "US"));
					nparcelas = a.select(".vtex-cpNumbersOfInstallment").first().text();
					//vparcelas = Digester.normalizaMoeda(a.select(".vtex-cpInstallmentValue").first().text().replaceAll(/R\$/, "").trim(), true);
					vparcelas = Digester.autoNormalizaMoeda(a.select(".vtex-cpInstallmentValue").first().text().replaceAll(/R\$/, "").trim(), true, new Locale("en", "US"));
					a.select('.vtex-cpProductUnavailable').each { span ->
						if( span.attr('class') != null && span.attr('class').indexOf('vtex-cpShow') > -1 )
							ativo = 'nao';
				}
			}
			
			if(_DEBUG_) println("\nsku="+sku+"\nimagem="+image+"\nlink="+url+"\nnome="+nome+"\nde="+oprice+"\npor="+fprice+"\nparcelas="+vparcelas+"\nvalor parcela="+nparcelas+"\n");

			if(sku != '' && url != '')
				Digester.salvaCatalogoComDescricao(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, "", nome, "", ativo, false, comentario);

                        if( ++cline % 100 == 0 )
                                conn = ConnHelper.get().reopen();
			
			foi = true;
                }
                
                if(foi == true) {
                	if(_DEBUG_) println("OK");
                	return true;
                } else {
                	if(_DEBUG_) println("falhou");
                }
		return false;
        }
        
        public static loadCategory(String category) {
		
		def cat = category.substring("http://www.shoestock.com.br/".length()); 
		def cats = cat.split("/");
		def categ1 = cats.length > 0 ? cats[0] : ""
		def categ2 = cats.length > 1 ? cats[1] : ""
		def categ3 = cats.length > 2 ? cats[2] : ""
		
		if(_DEBUG_) println("loading category: " + categ1 + "|" + categ2 + "|" + categ3 + "-->" + category);
		Document doc
		
                try {
			doc = Jsoup.connect(category).timeout(120000).userAgent("Mozilla").get();
                } catch(ex) {
                	if(_DEBUG_) println("fail");
                	return;
                }
                
                doc.select("script").each { str->
                	def tmp = str.toString();
                	def p = tmp.indexOf("buscapagina");
                	if(p > -1) {
                		def l = str.toString().substring(p);
				def k = l.indexOf('"');
				if(k == -1) k = l.indexOf("'");
                		def template = l.substring(0, k);
                		Boolean ret = true;
                		
                		for(int i=1; i<100 && ret; i++) {
	                        	ret = Shoestock.loadPage("http://www.shoestock.com.br/" + template + i, categ1, categ2, categ3);
	                        }
	                }
                }
                

        }

	public static void normaliza() {
		
		println("normalizando duplicidades ...");
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt1 = conn.prepareStatement("select max(id), link from catalogo where ativo = '1' group by link");
                ResultSet res = pStmt1.executeQuery();

                while( res.next() ) {
                        def key = res.getInt(1);
                        def link = res.getString(1);
			PreparedStatement pStmt = conn.prepareStatement("update catalogo set recomendacoes = ?, ativo = '0' where link = ? and id <> ?");
			pStmt.setString(1, ""+key);
			pStmt.setString(2, link);
			pStmt.setInt(3, key);
			print("(" + pStmt.executeUpdate() + ")");
			
		}

                ConnHelper.closeResources(pStmt1, res);

	}

	public static void main (String [] args ) {
		Connection conn = ConnHelper.get().reopen();
                        PreparedStatement pStmt = conn.prepareStatement("update catalogo set ativo = '0'");
                        pStmt.executeUpdate();
                        ConnHelper.closeResources(pStmt);

		
		if(_DEBUG_) println("crawling shoestock ...");
		
		Document doc
		
                try {
			doc = Jsoup.connect("http://www.shoestock.com.br").timeout(120000).userAgent("Mozilla").get();
                } catch(ex) {
			ex.printStackTrace();
			System.exit(0);
                }

                doc.select(".menu-departamento ul li a").each { a->
                	def url = a.attr("href");
                	if(url != null && !url.trim().equals(""))
                        	Shoestock.loadCategory( a.attr("href") );
                }

		Shoestock.normaliza();

//                Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
//                Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
//                Digester.normalizaSupernova(ConnHelper.get().reopen());
//                Digester.bestSellers(ConnHelper.get().reopen());
//                Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
	}

}
