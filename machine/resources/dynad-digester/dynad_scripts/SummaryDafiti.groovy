import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.NumberFormat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.mail.internet.*;
import javax.mail.*;
import javax.activation.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.xhtmlrenderer.pdf.ITextRenderer;
//import org.apache.poi.hslf.record.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;



@Grapes([
  @Grab(group='org.xhtmlrenderer', module='core-renderer', version='R8'),
  @Grab(group='com.lowagie', module='itext', version='2.0.8'),
  @Grab(group='org.apache.poi', module='poi', version=''),
  @Grab(group='poi', module='poi', version='2.5-final-20040302'),

  //@Grab(group='org.apache.poi', module='poi-ooxml', version='3.8'),
  //@Grab(group='org.apache.poi', module='poi-ooxml-schemas', version='3.8'),
  //@Grab(group='org.apache.poi', module='poi-scratchpad', version='3.8'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Summary {

	private static void toPDF(String txt) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		org.xhtmlrenderer.pdf.ITextRenderer renderer = new org.xhtmlrenderer.pdf.ITextRenderer();
		byte[] b;


		renderer.setDocumentFromString(txt.toString());
		renderer.layout();
		renderer.createPDF(baos);
		b = baos.toByteArray();
/*
		Document doc = createDocumentFromString(txt);
		renderer.setDocument(doc, null)
		renderer.layout();
		renderer.createPDF(baos);
		b = baos.toByteArray();
*/

		def file = new FileOutputStream( new File("/root/dynad_scripts/dynad_report.pdf" ) );
                def out = new BufferedOutputStream(file);
                out << b;
                out.close();
	}
	

	private static String buildReport (Date from, Date to ) { 
		String head = '<html><head><style TYPE="text/css">.gray {background: lightgray;color: black;font-weight: bold;text-align: center;width: 110px;border-left: 3px solid black;border-right: 1px solid black;border-bottom: 1px solid black;font-family: Verdana;}	.first {border-top: 3px solid black;} .last {border-bottom: 3px solid black !important;} .grayOdd {background: #AAA;color: black;font-weight: bold;	text-align: center;	width: 110px; border-left: 3px solid black;	border-right: 1px solid black; border-bottom: 1px solid black; font-family: Verdana; } .red { background: red; color: white; font-weight: bold; text-align: right; width: 110px; border-right: 3px solid black; border-left: 1px solid black; border-bottom: 1px solid black; font-family: Verdana;} .redOdd { background: #a00; color: white; font-weight: bold; text-align: right; width: 110px; border-right: 3px solid black; border-left: 1px solid black; border-bottom: 1px solid black; font-family: Verdana;} .green {	background: #080; color: white; font-weight: bold; text-align: right; width: 110px;	border-left: 2px solid black; border-right: 3px solid black; border-bottom: 3px solid black; font-family: Verdana;}	body { margin-left: 50px; margin-top:0;} .img { margin-bottom: 30px; } h1 { font-family: Arial;font-size:28px;}</style></head><body>';
		String trailler = '</body></html>';
		String title = '<tr><td colspan="3"><img class="img" src="logo_dynad.png"/></td></tr><tr><td colspan="3"><h1>Transacional Dafiti - ' + new SimpleDateFormat("yyyy-MM-dd").format(from) + ' até ' + new SimpleDateFormat("yyyy-MM-dd").format(to) + '</h1></td></tr>';
		String body = "";

		ConnHelper.host = 'cluster_db_host';
		ConnHelper.schema = 'dynad_dafiti';
		ConnHelper.usuario = 'dynad';
		ConnHelper.senha = 'danyd';
		Connection conn = ConnHelper.get().reopen();
		
		def sql = "select sum(impressions), date from summary_by_day where date between '" +new SimpleDateFormat("yyyy-MM-dd").format(from)+ "' and '" +new SimpleDateFormat("yyyy-MM-dd").format(to)+ "' group by date";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		ResultSet res = pStmt.executeQuery();
		def fmt = new SimpleDateFormat("dd/MMM");
		def odd = true
		Long sum = 0L;
		while( res.next() ) {
			String num = NumberFormat.getInstance().format(res.getLong(1));
			body += '<tr><td class="gray'+(odd?'Odd':'')+' '+(res.isFirst()?'first':(res.isLast()?'last':''))+'">' +fmt.format(res.getDate(2))+'</td><td class="red'+(odd?'Odd':'')+' '+(res.isFirst()?'first':(res.isLast()?'last':''))+'">' +num+ '</td><td></td></tr>';
			odd = !odd;
			sum += res.getLong(1);
		}
		conn.close();
		body += '<tr><td style="border-right: 1px solid black;"></td><td class="green">'+NumberFormat.getInstance().format(sum)+'</td><td></td></tr>';


		return head + '<table cellspacing="0">' + title + body + '</table>' + trailler;
	}
	
	public static void main (String [] args ) {
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -30);
		Date ini = cal.getTime();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date end = cal.getTime();

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		if(args.length == 2) {
			ini = (Date)formatter.parse(args[0]);
			end = (Date)formatter.parse(args[1]);
		}

		//println("from: " + formatter.format(ini) + " to: " + formatter.format(end));

		def str = buildReport(ini, end);
		toPDF(str);

		println("Report generated automatically by DynAd. See attached file.");

	}
	
}
