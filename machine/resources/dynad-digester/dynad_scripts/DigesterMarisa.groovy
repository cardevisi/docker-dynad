import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterMariza {
        static int key;
        static {
                ConnHelper.schema = "dynad_marisa";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3, gratis;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto, descricao;

                def summary = new XmlParser().parse(file);
                summary.channel.item.each {
                              	sku = it.'g:id'.text()
                                nome = Digester.stripTags(it.title.text())
                                descricao = Digester.stripTags(it.description.text())
                                url = it.link.text()

                                fprice = Digester.autoNormalizaMoeda(it.'g:sale_price'.text(), false, new Locale("pt", "BR"))
                                oprice = Digester.autoNormalizaMoeda(it.'g:price'.text(), false, new Locale("pt", "BR"))
                                nparcelas = it.'g:installment'.'g:months'.text()
                                vparcelas = Digester.autoNormalizaMoeda(it.'g:installment'.'g:amount'.text(), false, new Locale("pt", "BR"))

                        	image = it.'g:image_link'.text()
                         	promover = it.'g:availability'.text() == 'in stock' ? '1' : '0'
				String [] categorias = it.'g:product_type'.text().split('>');
                         	c1 = categorias[0];
                         	c2 = (categorias.length > 1 ? categorias[1] : null);
                         	c3 = (categorias.length > 2 ? categorias[2] : null);
                         	marca = it.'g:brand'.text();

					FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
                                        FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
                                        java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
                                        fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1 + '/' + c2, lookupChange: true, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
                                        fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
                                        fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA') );
                                        fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA') );

                                        DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
                                        if( ++cline % 100 == 0 )
                                                conn = ConnHelper.get().reopen();				

		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/mariza.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), true);
			Digester.bestSellers(ConnHelper.get().reopen());
                        Digester.validacao(ConnHelper.get().reopen());
                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

        }
	
}
