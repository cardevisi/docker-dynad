import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_linio";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3, category1;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                def list = summary.product

                list.each {
                	sku = it.@id
			category1 = it.category1.text();

                        stmt = """select id_sku from catalogo where sku = ?""";
                        pStmt.setString(1, sku);
                        ResultSet res = pStmt.executeQuery();
			if(res.next()) {
				def id_sku = res.getString(1);				
				stmt2 = """select id_categoria from codigos_categorias where categoria1 = ?""";
				pStmt2.setString(1, category1);
				ResultSet res2 = pStmt2.executeQuery();

				if(res2.next()) {
					def id_categoria = res2.getString(1);
					stmt3 = """insert into pre_categoria_email ( id_sku, id_categoria ) values ( ?, ? )""";
					pStmt3.setString(1, id_sku);
					pStmt3.setString(2, id_categoria);
					println("->" + pStmt3.executeUpdate());
				}
			}


                        stmt = """insert into pre_categorial_email (id_sku, id_categoria) values ( ?, ? );""";

			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
		
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/liniomx_email.xml' );
                        Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
                        Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
                        Digester.bestSellers(ConnHelper.get().reopen());
                        //Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

	}
	
}
