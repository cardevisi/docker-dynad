import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
 
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])



class SpiderEpoca {
	
	
	
	public static String toUTF8(s) {return new String(((String)s).getBytes("UTF-8")); }

	public static java.util.List<String> recuperarCategorias () {
		Document doc
		try {
				doc = Jsoup.connect("http://www.epocacosmeticos.com.br/default.aspx").timeout(120000).userAgent("Mozilla").get();
		} catch(ex) {
				ex.printStackTrace();
				System.exit(0);
		}
		
		java.util.List<String> categorias = new java.util.ArrayList<String>();
		doc.select(".bts_depts_header ul li a").each { a->
			categorias.add( a.attr("href") );
		}
		
		
		java.util.List<String> categorias2 = new java.util.ArrayList<String>();
		categorias.each { 
			try {
				doc = Jsoup.connect( it ).timeout(120000).userAgent("Mozilla").get();
				doc.select(".hTitle .link_menu").each { a->
					categorias2.add( a.attr("href") );
				}
			} catch(ex) {
					ex.printStackTrace();
					System.exit(0);
			}
		}
		
		return categorias2;
	}
	
	public static void recuperaProdutosDepto ( String url ) { 
		if( url.indexOf('-') == -1 ) return;
		String depto = url.tokenize("-")[-1];
		//depto = depto.substring(url.indexOf('-') + 1);
		depto = depto.substring(0, depto.indexOf('_D'));
		
		println( 'processando url: ' + url);
		
		ConnHelper.schema = "dynad_epoca";
		ConnHelper.usuario = "root";
		ConnHelper.senha = "1111";
		Connection conn = ConnHelper.get().reopen();
		
		try { 
			String urlPesquisa = 'http://www.epocacosmeticos.com.br/dept.aspx?idListGroup=&orderby=0&lstBrand=&lstPriceRange=&lstDept=&idDept='+depto+'&itensperpage=100&page=';
			for ( i in 1..100 ) {
			
				def achou = false;
				
				Document doc
				try {
						doc = Jsoup.connect(urlPesquisa + i).timeout(120000).userAgent("Mozilla").get();
				
				//log(doc);
				
				int index = 0;
				
				doc.select(".box ul li").each {li->
				
					def sku = li.attr("id");
					def name;
					def link;
					def image;
					def pricePromo;
					def price;
					def specialprice;
					def numParcelas;
					def parcela;
					def marca;
					def idProduto;
					def indisp = 'false';
					li.select(".conteudo_prods_listagem_new a").each { a ->
						link = a.attr('href');
						a.select("img").each { img ->
							image = img.attr('src');
						}
					}
					
					li.select(".tit_listagem_prods .verdana_12_cinza_upper strong").each {dive->
						marca = dive.text()
					}
					
					li.select(".tit_listagem_prods .verdana_12_cinza_escuro strong").each {dive->
						name = '';
						name += dive.text();
					}
					
					String t = (index.toString().length() == 1 ? '0' : '' ) + index;
					
					li.select("#ctl00_ContentSite_ctlDeptHomeControl_ctlListGroupControl_rptProducts_ctl"+ t +"_divHasPrice span").each {dive->
					
						if( price == null && dive.toString().indexOf('<s>') == -1 )
							price = dive.text();
					}
					
					li.select("#ctl00_ContentSite_ctlDeptHomeControl_ctlListGroupControl_rptProducts_ctl"+ t +"_divHasPromoPrice s").each {dive->
						if( pricePromo == null )
							pricePromo = dive.text();
					}
					
					
					li.select("#ctl00_ContentSite_ctlDeptHomeControl_ctlListGroupControl_rptProducts_ctl"+ t +"_divParcelsWithoutTax span strong").each {dive->
						if( numParcelas == null && dive.text().indexOf('x') > -1 ) {
							String [] d = dive.text().split(' ');
							numParcelas = d[0];
							parcela = d[2];
						}
					}
					
					li.select("#ctl00_ContentSite_ctlDeptHomeControl_ctlListGroupControl_rptProducts_ctl"+ t +"_dvUnavailable").each {dive->
						indisp = 'true';
					}
					
					if( price == null ) {
					
						li.select("#ctl00_ContentSite_ctlDeptControl_rptProducts_ctl"+ t +"_divHasPrice span").each {dive->
		
								if( price == null && dive.toString().indexOf('<s>') == -1 )
									price = dive.text();
							}
		
							li.select("#ctl00_ContentSite_ctlDeptControl_rptProducts_ctl"+ t +"_divHasPromoPrice s").each {dive->
								if( pricePromo == null )
									pricePromo = dive.text();
							}
		
		
							li.select("#ctl00_ContentSite_ctlDeptControl_rptProducts_ctl"+ t +"_divParcelsWithoutTax span strong").each {dive->
								if( numParcelas == null && dive.text().indexOf('x') > -1 ) {
									String [] d = dive.text().split(' ');
									numParcelas = d[0];
									parcela = d[2];
								}
							}
		
							li.select("#ctl00_ContentSite_ctlDeptControl_rptProducts_ctl"+ t +"_dvUnavailable").each {dive->
								indisp = 'true';
						}
					
					}
					
					if( price != null && price.indexOf('por ') > -1 ) price = price.substring(4);
					if( price != null && pricePromo == null ) pricePromo = price;
					
					try {
						depto = idProduto = link.tokenize("-")[-2];
						idProduto = link.tokenize("-")[-1];
						if( idProduto.indexOf('_p') > -1 )
							idProduto = idProduto.substring(0, idProduto.indexOf('_p'));
						else if( idProduto.indexOf('_P') > -1 )
							idProduto = idProduto.substring(0, idProduto.indexOf('_P'));
						else
							throw new RuntimeException('Codigo do produto nao disponivel');
							
					} catch ( Exception ex) {
						println('link: ' + link);
						throw new RuntimeException('falha no codigo do produto');
					}
					
					Integer id = null;
					PreparedStatement pStmt = conn.prepareStatement("select id from catalogo where produto = ?");
					pStmt.setString(1, idProduto);
					ResultSet res = pStmt.executeQuery();
					if( res.next() )
						id = res.getInt(1);
					
					ConnHelper.closeResources(pStmt, res);
					
					int idxCol = 0;
					
					String stmt = "insert into catalogo (produto, depto, nome, imagem, link, " + 
						"indisponivel, preco, preco_promocional, marca, numero_parcelas, valor_parcelas) values " + 
						" (?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?) ";
					if( id != null )
						stmt = "update catalogo set produto = ?, depto = ?, nome = ?, imagem = ?, link = ?, " +
						"indisponivel = ?, preco = ?, preco_promocional = ?, marca = ?, numero_parcelas = ?, valor_parcelas = ? " +
						" where id = ?";
					pStmt = conn.prepareStatement(stmt);
					
					pStmt.setString(++idxCol, idProduto);
					pStmt.setString(++idxCol, depto);
					pStmt.setString(++idxCol, name);
					pStmt.setString(++idxCol, image);
					pStmt.setString(++idxCol, link);
					pStmt.setString(++idxCol, indisp);
					pStmt.setString(++idxCol, price);
					pStmt.setString(++idxCol, pricePromo);
					pStmt.setString(++idxCol, marca);
					pStmt.setString(++idxCol, numParcelas);
					pStmt.setString(++idxCol, parcela);
					if( id != null )
						pStmt.setInt( ++idxCol , id);
					pStmt.executeUpdate();
					ConnHelper.closeResources(pStmt, null);
						
					println('"' + ( id == null ? 'add' : 'update') +
							'";"' + name +
							'";"' + depto +
							'";"' + idProduto +
							'";"' + image +
							'";"' + link +
							'";"' + indisp +
							'";"' + price +
							'";"' + pricePromo +
							'";"' + marca +
							'";"' + numParcelas +
							'";"' + parcela + '"' );
					index++;
					i++;
					achou = true;
					
				}
				
				if(!achou)
					break;
				
				} catch(ex) {
						ex.printStackTrace();
						//System.exit(0);
				}
			}
		} finally { 
			ConnHelper.closeResources(conn);
		}
		
		return;
	}
	
	public static void executaProc () {
		CallableStatement callableStatement = null;
		Connection conn = ConnHelper.get().reopen();
		try { 
			callableStatement = conn.prepareCall(" { call atualizar_valores(?) } ");
			callableStatement.setInt(1, 100 );
			callableStatement.execute();
		} finally { 
			if( callableStatement != null ) callableStatement.close();
			ConnHelper.closeResources(conn);
		}
	}
	
	public static void main (String [] args ) {
		/**
		 * Carrega todo o catálogo disponível no site da Dafiti. 
		 */
		recuperarCategorias().each { 
			recuperaProdutosDepto( it );
		}
		
		//executaProc();
		
		/**
		 * marca produtos ativos na tabela catalogo
		 */
		//markXml( downloadXml( true ) );
		
		/**
		 * Verifica todos os produtos cadastrados na tabela catalogo com o campo recomendacoes em branco
		 */
		//atualizarRecomendacoes();
	}
	
}
