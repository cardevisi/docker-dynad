import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
	static int key;
	static {
		ConnHelper.schema = "dynad_mobly";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
	}
	
	public static boolean _DEBUG_ = false;
	
	private static markXml ( String file ) { 

		ConnHelper.schema = "dynad_mobly";
                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover;

                def summary = new XmlParser().parse(file);
                def list = summary.produto
		list.each {
/**
<produto>
<sku>AB011CH14OZPMOB</sku>
<nome>Longarina Evidence Executiva 2 Lugares Preta Absolut</nome>
<link_imagem>http://static.mobly.com.br/p/1-0001-585511-1-product.jpg</link_imagem>
<link_do_produto>http://tracking.searchmarketing.com/click.asp?aid=120159870000060969</link_do_produto>
<preco>399.99</preco>
<preco_especial>219.99</preco_especial>
<parcelas>4</parcelas>
<valor_parcela>54.99</valor_parcela>
<marca>Absolut</marca>
<categoria>Escritório</categoria>
<subcategorias>Cadeiras de Escritório</subcategorias>
</produto>
**/

			c1 = c2 = c3 = null;
			sku = it.sku.text()
			nome = Digester.stripTags( it.nome.text() );
//			nome = Digester.ISO2UTF8(it.nome.text())
			url = it.link_do_produto.text()
			fprice = Digester.autoNormalizaMoeda(it.preco_especial.text(), false, new Locale("pt", "BR"))
			oprice = Digester.autoNormalizaMoeda(it.preco.text(), false, new Locale("pt", "BR"))
			nparcelas = it.parcelas.text()
			vparcelas = Digester.autoNormalizaMoeda(it.valor_parcela.text(), false, new Locale("pt", "BR"))
			image = it.link_imagem.text()
			marca = it.marca.text()
			c1 = it.categoria.text()
			c2 = it.subcategorias.text()

			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA') );
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);				
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();

		}
		
	}
	
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/mobly.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			////atualizarRecomendacoes();
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
			//Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
			//Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                }

		 ///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select sku,id,nome,ativo,imagem,link,preco_original,preco_promocional,status,numero_parcelas,valor_parcelas,categoria,recomendacoes from catalogo where sku IS NOT NULL and nome IS NOT NULL and imagem IS NOT NULL and link IS NOT NULL and preco_original IS NOT NULL and preco_promocional IS NOT NULL and status IS NOT NULL and numero_parcelas IS NOT NULL and valor_parcelas IS NOT NULL and categoria IS NOT NULL and recomendacoes IS NOT NULL");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/mobly.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2)+"|"+res.getString(3)+"|"+res.getString(4));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(5)));
                                for(int i=6; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
                                out.print("\n");
                                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
                        }
                }
                ConnHelper.closeResources(pStmt, res);


	}
	
}
