import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class DigesterMariza {
    static int key;
    static {
        ConnHelper.schema = "dynad_submarino";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if (res.next()) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }

    public static boolean _DEBUG_ = false;

    private static markXml(String file) {
        Connection conn = ConnHelper.get().reopen();
        int cline = 0;
        def p = null;
        String line = null;

        String sku, c1, c2, c3, gratis;
        String image, oprice, fprice, nparcelas, vparcelas;
        String url, marca, nome, codigo, promover, desconto, descricao;

        def summary = new XmlParser().parse(file);
        summary.channel.item.each {
            /*
            <item>
                <g:id>119048</g:id>
                <title><![CDATA[CD Sade - The Best Of Sade]]></title>
                <description><![CDATA[The Best of Sade é um álbum dos melhores êxitos do grupo inglês Sade, lançada em 12 de Novembro de 1994 no Reino Unido e em 26 de Novembro do mesmo ano nos Estados Unidos, pelo selo Epic Records.Compilação dos maiores hits da cantora nigeriana Sade, que mistura soul, jazz, pop. Destaque para "The Sweetest Taboo", "Smooth Operato", "No Ordinary Love", "Paradise", entre outras.Todas as informações divulgadas são de responsabilidade exclusiva do fabricante/fornecedor.Imagens meramente ilustrativas.]]></description>             
                <g:price>R$ 17,90</g:price>
                <g:condition>new</g:condition>
                <g:gtin>5099747779329</g:gtin>
                <link><![CDATA[http://www.americanas.com.br/produto/119048/cd-sade-the-best-of-sade?epar=4AD7-0B94-6391&opn=AFLACOM&WT.mc_id=lomadeexml]]></link>
                <g:image_link><![CDATA[http://iacom.s8.com.br/produtos/01/00/item/119/0/119048G1.jpg]]></g:image_link>
                <g:product_type><![CDATA[Música > R&B / Soul > CD]]></g:product_type>
                <c:category_id>485866</c:category_id>
                <g:category><![CDATA[Música]]></g:category>
            </item>
            <item>
                <g:id>119056</g:id>
                <title><![CDATA[CD Oasis -(what`s The Story) Morning Glory?]]></title>
                <g:price>R$ 17,90</g:price>
                <g:condition>new</g:condition>
                <g:gtin>5099748102027</g:gtin>
                <link><![CDATA[http://www.americanas.com.br/produto/119056/cd-oasis-what-s-the-story-morning-glory-?epar=4AD7-0B94-6391&opn=AFLACOM&WT.mc_id=lomadeexml]]></link>
                <g:image_link><![CDATA[http://iacom.s8.com.br/produtos/01/00/item/119/0/119056P1.jpg]]></g:image_link>
                <g:product_type><![CDATA[Música > Rock Internacional > CD]]></g:product_type>
                <c:category_id>485866</c:category_id>
                <g:category><![CDATA[Música]]></g:category>
            </item>
            */
            sku = it.'g:id'.text()
            nome = Digester.stripTags(it.title.text())
            descricao = ''
            if(it.description.text()) Digester.stripTags(it.description.text())
            if(descricao.length() > 512)descricao = descricao.substring(0, 512);
            url = it.link.text()
            if(url.indexOf("?") > -1) {
                def c = -1
                if((c = url.indexOf('&utm_source')) > -1) url = url.substring(0, c);
                if((c = url.indexOf('&utm_campaign')) > -1) url = url.substring(0, c);
                if((c = url.indexOf('&utm_medium')) > -1) url = url.substring(0, c);
            }
            fprice = Digester.autoNormalizaMoeda(it.'g:price'.text(), false, new Locale("pt", "BR"))
            oprice = Digester.autoNormalizaMoeda(it.'g:price'.text(), false, new Locale("pt", "BR"))
            nparcelas = '1'
            vparcelas = fprice

            image = it.'g:image_link'.text()
            promover = '1'
            String[] categorias = it.'g:product_type'.text().split('>');
            c1 = categorias[0];
            c2 = (categorias.length > 1 ? categorias[1] : null);
            c3 = (categorias.length > 2 ? categorias[2] : null);
            
            marca = ''
            if ( it.'g:brand'.text() ) marca = it.'g:brand'.text();

            FieldMetadata fSku = new FieldMetadata(columnName: 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
            FieldMetadata fAtivo = new FieldMetadata(columnName: 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
            java.util.List < FieldMetadata > fields = new java.util.ArrayList < FieldMetadata > ();
            fields.add(new FieldMetadata(columnName: 'categoria', columnType: 'string', columnValue: c1 + '/' + c2, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG'));
            fields.add(new FieldMetadata(columnName: 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK'));
            fields.add(new FieldMetadata(columnName: 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA'));

            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
            if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
        }
    }

    public static void main(String[] args) {
        if (args.length == 0 || args[0] != 'BEST_SELLERS') {
            markXml('/mnt/XML/submarino.xml');
            //Digester.normalizaSupernova(ConnHelper.get().reopen());
            //Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
            //Digester.completaRecomendacoes(ConnHelper.get().reopen(), true);
            //Digester.bestSellers(ConnHelper.get().reopen());
            //Digester.validacao(ConnHelper.get().reopen());
        } else if (args[0] == 'BEST_SELLERS') {
            Digester.bestSellers(ConnHelper.get().reopen());
        }
    }

}
