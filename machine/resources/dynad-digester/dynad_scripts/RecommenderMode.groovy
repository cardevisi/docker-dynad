public enum RecommenderMode {
	NONE, SAME_PRICE, BEST_PRICE, DIFFERENT_PRICE, DIFFERENT_LINK, DIFFERENT_NAME, DIFFERENT_NAMELINK, GEO_LOCATION, GEO_LOCATION_DIFFERENT_CITY
}

