
class MainXml {
	
	public static void main ( String [] args ) {
		String address = 'http://blog.dafiti.com.br/parceiros/asapcode/xml_asapcode.xml';
		/**
		//ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		def file = new FileOutputStream(address.tokenize("/")[-1])
		def out = new BufferedOutputStream(file)
		out << new URL(address).openStream();
		out.close();**/
		
		def file = new FileOutputStream('saida_asapcode.csv', true);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter( file, 'UTF-8' ) )

		
		int cline = 0;
		def p = null;
		new File( address.tokenize("/")[-1] ).eachLine() { line -> 
			line = new String( line.getBytes('UTF-8'), 'UTF-8' );
			
			if( line == '<products>' || line == '</products>' ) return;
			
			if( line.startsWith('<product ') ) { 
				p =  new product( id : getProductId( line ) );
				return;
			}  
			
			if( line.startsWith('<name>') ) p.name = getField( line );
			if( line.startsWith('<smallimage>') ) p.smallImage = getField( line );
			//if( line.startsWith('<bigimage>') ) p.bigImage = getField( line );
			if( line.startsWith('<producturl>') ) p.productUrl = getField( line );
			if( line.startsWith('<originalprice>') ) p.originalPrice = getField( line );
			if( line.startsWith('<finalprice>') ) p.finalPrice = getField( line );
			if( line.startsWith('<nparcelas>') ) p.nParcelas = getField( line );
			if( line.startsWith('<vparcelas>') ) p.vParcelas = getField( line );
			if( line.startsWith('<marca>') ) p.marcas = getField( line );
			
			
			if( line == '</product>' ) {
				out.write( p.get() );
				p = null;
				return; 
			}
		}
		
		out.flush();
		out.close();
		
		/**
		def pers=new XmlSlurper().parse(new File( address.tokenize("/")[-1] ) );
		pers.children().each { product -> 
			println( product.name) ;
		}**/
		
	}
	
	static String getField ( String buffer ) {
		buffer = buffer.substring( buffer.indexOf( '<![CDATA[' ) + 9 );
		buffer = buffer.substring( 0, buffer.indexOf( ']]>' ));
		return buffer;
	}
	  
	static String getProductId ( String buffer ) {
		buffer = buffer.substring( buffer.indexOf( 'id="' ) + 4 );
		buffer = buffer.substring( 0, buffer.indexOf( '"' ));
		return buffer;
	}

}

class product {
	String id;
	String name;
	String smallImage;
	String bigImage;
	String productUrl;
	String originalPrice;
	String finalPrice;
	String nParcelas;
	String vParcelas;
	String marcas;
	
	String get () {
		if( vParcelas != null && vParcelas.indexOf(',') > -1 )
			vParcelas = vParcelas.replaceAll(",", ".");
		if( originalPrice != null && originalPrice.indexOf(',') > -1 )
			originalPrice = originalPrice.replaceAll(",", ".");
		if( finalPrice != null && finalPrice.indexOf(',') > -1 )
			finalPrice = finalPrice.replaceAll(",", ".");

		return '"' + id + '";"' +
			name + '";"' +
			smallImage + '";"' +
			productUrl + '";"' +
			originalPrice + '";"' +
			finalPrice + '";"' +
			nParcelas + '";"' +
			vParcelas + '";"' +
			marcas + '"' + "\r\n";
		
	}
	
}
