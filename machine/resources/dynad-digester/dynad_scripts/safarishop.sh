echo "INICIO - SafariShop"

date
START=$(date +%s)

### PARAMETROS ###
#XML_URL=http://www.tricae.com.br/zanox.xml
#XML_URL="http://xml.safarishop.com.br/18/Carteira03/asapcod_XML_QE/QueimaoDeEstoque.xml"
XML_URL="http://xml.safarishop.com.br/18/Carteira02/UOL_SLRTG_XML-ASAPCODE/QueimaoDeEstoque.xml"
XML_FILE=/mnt/XML/safarishop.xml
TEMP_NAME=safarishop
SCHEMA=dynad_safarishop
DB_HOST=64.31.10.123
EXP_HOST="64.31.10.123 23.23.92.125 54.232.127.115 68.233.252.114 216.144.240.50 54.163.247.172 54.232.127.115 54.232.127.124"
DIGESTER=/root/dynad_scripts/DigesterSafarishop.groovy

SNFILE=/mnt/safarishop.txt
SNSERVER="200.147.166.24 200.147.166.25 200.147.166.26 200.147.166.27"
SNDATASOURCE=safarishop
##################

RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

echo "baixando xml ..."
RES1=`sh download.sh /mnt/XML/safarishop1.xml http://xml.safarishop.com.br/18/Carteira02/UOL_LPRTG_XML-ASAPCODE/QueimaoDeEstoque.xml`
RES2="0"
RES3="0"
#RES2=`sh download.sh /mnt/XML/safarishop2.xml http://xml.safarishop.com.br/18/carteira02/UOL_SLRTG_XML-CANAIS-ASAPCODE/OfertasGeralCanais.xml`
#RES3=`sh download.sh /mnt/XML/safarishop3.xml http://xml.safarishop.com.br/18/carteira02/UOL_SLRTG_XML-LOJA-ASAPCODE/Produtos.xml`

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE

RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi


if [ "$RES1" != "1" -o "$RES2" != "1" -o "$RES3" != "1" ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
     echo "exportando para $ip"
     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done

  echo "atualizando supernova 2 ..."
  for ip in $SNSERVER; do
     echo "exportando para $ip"
     curl -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
  done

else

  if [ $RANKING -eq 1 ]
  then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
       echo "exportando para $ip"
       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done

    echo "atualizando supernova 2 ..."
    for ip in $SNSERVER; do
       echo "exportando para $ip"
       curl -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
    done

  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"

