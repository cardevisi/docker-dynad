import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;


class ConsProd {
	protected LinkedList list = new LinkedList();
	protected int MAX = 10;
	protected boolean done = false; // Also protected by lock on list.
	
	Producer producer;
	java.util.List<Consumer> consumers = null;
	
	BufferedWriter out = null;
	int cLine = 0;
	Object _LOCK_WRITER = new Object();
	
	public ConsProd ( int workers ) { 
		producer = new Producer();
		
		out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream( new java.io.File('/tmp/saida_complementar.out'), true ), 'UTF-8' ) );
		int cLine = 0;
		
		consumers = new java.util.ArrayList<Consumer>();
		for(int x = 0; x < workers; x++)
			consumers.add ( new Consumer() );
	}
	
	
	/** Inner class representing the Producer side */
	class Producer extends Thread {
		Reader  r = null;
		public Producer () {
			r = new InputStreamReader(new FileInputStream("/tmp/saida.txt"), "UTF-8");
			start();
		}
		
		private String getNextLine () {
			return r.readLine();
		}
		
	  public void run() {
		while (true) {
		  // Get request from the network - outside the synch section.
		  // We're simulating this actually reading from a client, and it
		  // might have to wait for hours if the client is having coffee.
		  synchronized(list) {
			  while (list.size() == MAX) // queue "full"
			  try {
				//System.out.println("Producer WAITING");
				list.wait();   // Limit the size
			  } catch (InterruptedException ex) {
				System.out.println("Producer INTERRUPTED");
			  }
			  
			String line = getNextLine();
			if( line != null && !line.trim().equals('') ) {
				//println('added line: ' + line);
				list.addFirst( line );
				list.notifyAll();  // must own the lock
				//System.out.println("Produced 1; List size now " + list.size());
			}
			
			if (done)
			  break;
		  }
		}
	  }
	  
	}
	
	
	/** Inner class representing the Consumer side */
	class Consumer extends Thread {
		public Consumer () { 
			start();
		}
	  public void run() {
		while (true) {
		  Object obj = null;
		  synchronized(list) {
			while (list.size() == 0) {
			  try {
				//System.out.println("CONSUMER WAITING");
				list.wait();  // must own the lock
			  } catch (InterruptedException ex) {
				System.out.println("CONSUMER INTERRUPTED");
			  }
			}
			obj = list.removeLast();
			list.notifyAll();
			int len = list.size();
			//System.out.println("List size now " + len);
			if (done)
			  break;
		  }
		  process(obj);  // Outside synch section (could take time)
		  //yield(); DITTO
		}
	  }
  
	  void process(Object obj) {
		  String line = obj.toString();
		  String [] data = line.split(';');
		  
		  try { 
			  data[4];
		  }catch( java.lang.ArrayIndexOutOfBoundsException e ) {
		  	println ( data );
		  	throw e;
		  }
		  String url = data[4].replaceAll('"', '').replaceAll('"', '');
		  
		  String codigo = url.tokenize("/")[-1];
		  if( codigo.indexOf('?') > -1 )
			  codigo = codigo.substring(0, codigo.indexOf('?'));
		  codigo = codigo.tokenize("-")[-1];
		  
		  if( codigo.indexOf('.') > -1 )
			  codigo = codigo.substring(0, codigo.indexOf('.'));
			  
		  //println( 'processando codigo: ' + codigo );
		  int index = 0;
		  StringBuilder sb = new StringBuilder();
		  recomendacoes( 'http://www.dafiti.com.br/recommendation/recommendation/?c=catalog&a=detail&category=0&brand=0&color=0&price=0&upper_material=0&sole_material=0&inner_material=0&heel_shape=0&heel_height=0&trodden_type=0&gender=0&prod=' + codigo + '&new_products=0&special-price=0' ).each { product ->
			  if( index++ > 0 ) {
				  sb.append( ';' );
			  } else {
				  sb.append( line.trim() ).append(';');
			  }
			  sb.append( product.get() );
		  };
		  sb.append( "\r\n" );
		  synchronized (_LOCK_WRITER) {
			  print( ( ++cLine ) + ' - ' + sb.toString() );
			  out.write( sb.toString() );
			  out.flush();
		  }
		  
	  }
	  
	  
	  
	  java.util.List<ShortProduct> recomendacoes (String url ) {
		  
		  java.util.List<String> skus = new java.util.ArrayList<String>();
		  
		  StringBuilder sb = null;
		  while( true ) {
			  try {
				  sb = new StringBuilder();
				  url.toURL().withReader { reader ->
					  sb.append( reader.readLine() );
				  }
				  break;
			  } catch ( java.net.SocketTimeoutException ex) { }
		  }
  
		  int posi = -1;
		  while( (posi = sb.indexOf('sku=') ) > -1 ) {
			  sb.delete(0, posi + 4);
			  if( skus.indexOf( sb.substring(0, sb.indexOf('&') ) ) == -1 )
				  skus.add ( sb.substring(0, sb.indexOf('&') ) );
			  sb.delete(0, sb.indexOf('&') + 1);
		  }
		  
		  java.util.List<ShortProduct> product = new java.util.ArrayList<ShortProduct>();
		  int maxIndex = 0;
		  for(String sku : skus) {
			  product.add ( detalhes( sku ) );
			  if( ++maxIndex == 3 ) break;
		  }
			  
		  return product;
	  }
	  
	  ShortProduct detalhes ( String sku ) {
		  String url = 'http://www.dafiti.com.br/catalog/recommendationview/?sku=' + sku + '&tracking=recommendations&pageName=product&position=3&isVertical=1';
		  StringBuilder sb = null;
		  while( true ) {
			  try {
				  sb = new StringBuilder();
				  url.toURL().eachLine { line ->
					  sb.append( new String( line.getBytes(), 'UTF-8') );
				  }
				  break;
			  } catch ( java.net.SocketTimeoutException ex) { }
		  }
		  
		  TagNode rootNode;
		  HtmlCleaner cleaner = new HtmlCleaner();
		  rootNode = cleaner.clean( sb.toString() );
		  
		  
		  
		  String imagem = null, brand = null, productName = null, productUrl = null, price = null, rates = null, specialPrice = null, nParcelas, vParcelas;
		  TagNode [] divElements = rootNode.getElementsByAttValue('class', 'image', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) imagem = divElements[i].getAttributeByName('id');
		  
		  divElements = rootNode.getElementsByAttValue('class', 'catalog_brand', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) brand = divElements[i].text.toString();
		  divElements = rootNode.getElementsByAttValue('class', 'product-image', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) {
			  productName = divElements[i].getAttributeByName('title');
			  productUrl = 'http://www.dafiti.com.br' + divElements[i].getAttributeByName('href');
		  }
		  
		  divElements = rootNode.getElementsByAttValue('class', 'price', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) price = divElements[i].text.toString();
		  
		  divElements = rootNode.getElementsByAttValue('class', 'specialprice', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) specialPrice = divElements[i].text.toString();
  
		  divElements = rootNode.getElementsByAttValue('class', 'catalog-rates', true, true);
		  for (int i = 0; divElements != null && i < divElements.length; i++) rates = divElements[i].text.toString().trim();
  
		  if( specialPrice == null ) specialPrice = price;
		  
		  if( rates != null ) { 
			  nParcelas = rates.substring(0, rates.indexOf(' x' )).trim();
			  vParcelas = rates.substring(rates.indexOf('R$') + 2).trim();
		  }
		  
		  if( price != null && price.indexOf('R$ ') > -1 ) price = price.substring(price.indexOf('R$ ') + 3); 
		  if( specialPrice != null ) specialPrice = specialPrice.substring(specialPrice.indexOf('R$ ') + 3);
		  
		  return new ShortProduct(
			  id : sku,
			  image : imagem,
			  marcas : brand,
			  name : productName,
			  url : productUrl,
			  price: price,
			  nParcelas : nParcelas,
			  vParcelas : vParcelas,
			  rate : rates,
			  specialPrice : specialPrice
		  );
	  }
	  
	}
	
	
	
	public static void main ( String [] args ) { 
		ConsProd m = new ConsProd( 10 );
		
	}
	
	
	
}
