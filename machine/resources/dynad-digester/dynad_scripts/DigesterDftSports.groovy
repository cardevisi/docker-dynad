import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterDiSantinni {
	static int key;
	static {
		ConnHelper.schema = "dynad_dafiti_sports";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;

        def rss = new XmlParser().parse(file);
        def list = rss.product
        
		list.each {

/*

                <product id="FI477ACF35GPK">
                        <name><![CDATA[Bolsa Fila Shine Preto]]></name>
                        <smallimage><![CDATA[http://static.dafity.com.br/p/-4683201-1-catalog.jpg]]></smallimage>
                        <bigimage><![CDATA[http://static.dafity.com.br/p/-4683201-1-zoom.jpg]]></bigimage>
                        <producturl><![CDATA[http://www.dafitisports.com.br/Bolsa-Fila-Shine-Preto-1023864.html?re=1294241807.feminino.bolsaseacessorios_mochilasemalas.1023864&utm_source=1294241807&utm_medium=re&utm_term=Bolsa-Fila-Shine-Preto-1023864&utm_content=mochilasemalas&utm_campaign=feminino.bolsaseacessorios_mochilasemalas]]></producturl>
                        <originalprice><![CDATA[89.90]]></originalprice>
                        <finalprice><![CDATA[89.90]]></finalprice>
                        <nparcelas><![CDATA[3]]></nparcelas>
                        <vparcelas><![CDATA[29.97]]></vparcelas>
                        <marca><![CDATA[Fila]]></marca>
                        <category1><![CDATA[bolsas e acessorios]]></category1>
                        <category2><![CDATA[mochilas e malas]]></category2>
                        <category3><![CDATA[feminino]]></category3>
                        <category4><![CDATA[acessorios-femininos]]></category4>
                        <category5><![CDATA[mochilas-e-bolsas]]></category5>
                        <instock><![CDATA[sim]]></instock>
                    <sizes></sizes>
                </product>


*/

			sku = it.@id
			nome = Digester.ISO2UTF8(it.name.text());
		        url = it.producturl.text()
			if(url.indexOf("?") > -1)
				url = url.substring(0, url.indexOf("?"));

			oprice = it.originalprice.text()
			//oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"));
			oprice = oprice;
			
			fprice = it.finalprice.text()
			//fprice = Digester.autoNormalizaMoeda(fprice, false, new Locale("pt", "BR"));
			fprice = fprice;

			nparcelas = it.nparcelas.text();                    
			//vparcelas = Digester.autoNormalizaMoeda(it.vparcelas.text(), false, new Locale("pt", "BR"));
			vparcelas = it.vparcelas.text()
			image = it.bigimage.text();

			c1 = it.category1.text();
			c2 = it.category2.text();
			c3 = it.category3.text();

			marca = it.marca.text();

			categoria = c1 + '/' + c2 + '/' + c3;

			fprice = fprice==null||fprice==""?"0,00":fprice;
			oprice = oprice==null||oprice==""?"0,00":oprice;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;
                
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: true, platformType: 'NA') );

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/dafiti_sports.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
			DigesterV2.validacao(ConnHelper.get().reopen(), 5000, 10.0);
        } else
	        if(args[0] == 'BEST_SELLERS') {
                Digester.bestSellers(ConnHelper.get().reopen());
	        }


                ///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/dafitisports.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount()-1; i++) out.print("|"+res.getString(i).replace("|", " "));
                                def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
                                        PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
                                        ResultSet res2 = pStmt2.executeQuery();
                                        def recs = "";
                                        while( res2.next() ) { recs += (recs==""?"":",") + res2.getString(1); }
                                        ConnHelper.closeResources(pStmt2, res2);
                                out.print("|"+recs);
                                out.println("\n_" + res.getString(2) + "|" + res.getString(1));
                        }
                }
                ConnHelper.closeResources(pStmt, res);

    }
}


