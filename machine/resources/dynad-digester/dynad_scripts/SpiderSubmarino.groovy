import groovy.json.JsonSlurper;


class SpiderSubmarino {
	
	public static void main ( String [] args ) { 
		def urlString = "http://www.submarinoviagens.com.br/hoteis/UIService/Service.svc/SearchHotels"
		def queryString = '{"req":{"PointOfSale":"SUBMARINO","SearchData":{"SearchMode":1,"AirSearchData":null,"HotelSearchData":{"LocationId":-1,"Location":"Porto Seguro / BA, Brasil","CheckIn":{"Year":"2012","Month":"05","Day":"05","isValid":true},"CheckOut":{"Year":"2012","Month":"05","Day":"06","isValid":true},"AnotherDate":null,"RoomsRequest":[{"Adults":"2","ChildAges":[],"SearchIndex":1}],"Sources":[],"Source":0,"MarkupType":1},"AttractionSearchData":null,"PointOfSale":"SUBMARINO"},"UserBrowser":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0"}}';
	
		def url = new URL(urlString)
		def connection = url.openConnection()
		connection.setRequestProperty("Accept", "text/plain, */*");
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
		
		connection.setRequestMethod("POST")
		connection.doOutput = true
	
		def writer = new OutputStreamWriter(connection.outputStream)
		writer.write(queryString)
		writer.flush()
		writer.close()
		connection.connect()
		def content = connection.content.text;
		def json = new JsonSlurper().parseText(content);
		println( json.SessionId );
		println( json.PullStatusFrom );
		println( json.SearchId );
		println( content );
		
		carregarResultado( json.SearchId, json.PullStatusFrom);
	}
	
	public static void carregarResultado (String searchId, String pullStatusFrom) { 
		println( '*******************************************************');
		println( '*** loading results                                   *')
		println( '*******************************************************');
		
		
		def urlString = "http://www.submarinoviagens.com.br/hoteis/UIService/Service.svc/GetSearchStatus"
		def queryString = '{"req":{"SearchId":"'+searchId+'","PointOfSale":"SUBMARINO","UserBrowser":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0"},"pullStatusFrom":"'+pullStatusFrom+'"}';
	
		def url = new URL(urlString)
		def connection = url.openConnection()
		connection.setRequestProperty("Accept", "text/plain, */*");
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
		
		connection.setRequestMethod("POST")
		connection.doOutput = true
	
		def writer = new OutputStreamWriter(connection.outputStream)
		writer.write(queryString)
		writer.flush()
		writer.close()
		connection.connect()
		def content = new String( connection.content.text.bytes, "utf-8");
		
		def json = new JsonSlurper().parseText(content);
		int index = 0;
		json.Hotels.each { hotel ->
			println('-----------------------------------');
			println( 'idx: ' + (++index) );
			println( 'name: ' + hotel.Name );
			println( 'address: ' + hotel.Address.Street );
			println( 'category: ' + hotel.Category );
			println( 'location: ' + hotel.Location );
			println( 'location id: ' + hotel.LocationId );
			println( 'promo fare: ' + hotel.PromoDisplayDailyFare );
			try { 
				println( 'thumb: ' + hotel.ThumbImage.Url );
			} catch ( java.lang.NullPointerException ex ){ 
				println('thumb: http://www.submarinoviagens.com.br/hoteis/Styles/images/hotel/SUBMARINO/thumb-hotel-nao-disp.gif' );
			}
		}
		
		//println( content );
	}
}
