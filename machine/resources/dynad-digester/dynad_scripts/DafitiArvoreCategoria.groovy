import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.net.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab('org.jsoup:jsoup:1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Dafiti {

	static final Integer MAX_ELEMENTS_TO_POPULATE = 50;
	
        static int key;
        static {

                ConnHelper.schema = "dynad_dafiti";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);

        }


	static void populaSkusNaCategoria() {
		def conn = ConnHelper.get().reopen()
		Digester.categoriasSite(conn, { ctg -> 
		    try {
			Document doc
			def response = ""
			def pg = "http://www.dafiti.com.br/" + URLDecoder.decode(ctg, "UTF-8");

			def path = ctg
			def categ = ctg

			PreparedStatement pStmt22 = conn.prepareStatement("delete from sku_categoria_site where id_categoria_site in (select id from categoria_site where categoria = ?) ");
			pStmt22.setString(1, path);
			def cntDel = pStmt22.executeUpdate();

			if(path.lastIndexOf('%2F') > -1) path = path.substring(path.lastIndexOf('%2F')+3);
			path = URLDecoder.decode(path);
print("\n" + URLDecoder.decode(categ) + ":");
			PreparedStatement pStmt = conn.prepareStatement("select sku from catalogo where ativo = '1' and lower(marca) = '" + path.toLowerCase().trim() + "' order by score desc limit 1," + MAX_ELEMENTS_TO_POPULATE);
			ResultSet res = pStmt.executeQuery();
			def conta = 0;
       	        	while( res.next() && conta < MAX_ELEMENTS_TO_POPULATE) {
       	             		def sku = res.getString(1);
				print("+");
				if(conta++<MAX_ELEMENTS_TO_POPULATE) {
					Digester.salvaCategoriaSite(conn, ctg, sku ) == 0 ? conta-- : conta;
				} else return;
			}

	
			def entrou = true;
//print("tot:----->" + entrou + "----->" + conta);
			for(int i=1; i<10&&conta<=MAX_ELEMENTS_TO_POPULATE&&entrou; i++) {
				entrou = false;
				def page = i>1?"?page="+i:""
				try {
					//response = Jsoup.connect(pg+page).timeout(120000).execute();
					response = Jsoup.connect(pg+page)
		    .userAgent("Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5")
                    .timeout(10000)
                    .followRedirects(true)
                    .ignoreHttpErrors(true)  // <--- Underlined red in eclipse plus the error msg
                    .execute();

//print("url:----->" + response.statusCode() + "----->" + i + "----->" + categ.indexOf("%2F") + "----->" + categ + "\n");
					if(response.statusCode() != 200) {
						if(i > 1) break;
						if(categ.indexOf("%2F") <= 0 && conta > 0) { // eh uma marca, faz a busca
							pg = "http://www.dafiti.com.br/catalog/?category=&q=" + URLDecoder.decode(categ, "UTF-8") + "&wtqs=1";
							entrou=true;
							i = 0; 
							continue;
						}
print(".");
						if(categ.indexOf("%2F") > 0) {
							categ = categ.substring(categ.indexOf("%2F") + 3);
							if(categ == "") break;
							pg = "http://www.dafiti.com.br/" + URLDecoder.decode(categ, "UTF-8");
							entrou=true;
							i = 0; 
							continue;
						}
					}  
//println(response.statusCode() + "-->" + pg+page);
	       	                 	doc = response.parse();
       		         		doc.select(".product-li").each { prod->
						entrou = true;
       		                 		def sku = prod.attr("id")
print("o");
//print("[" + sku + "]");
						if(conta++<MAX_ELEMENTS_TO_POPULATE) {
							Digester.salvaCategoriaSite(conn, ctg, sku ) == 0 ? conta-- : conta;
						} else return;
/*
PreparedStatement pStmt2233 = conn.prepareStatement("select nome from catalogo where sku = ?");
pStmt2233.setString(1, sku);
ResultSet resss = pStmt2233.executeQuery();
if(resss.next()) println(resss.getString(1));
resss.close(); pStmt2233.close();
*/

					}
				} catch(ex) {
println("PAUUUUUUUUUUUUUU");
					break;
		                }
			}

            	    } catch(Exception ex) {ex.printStackTrace();}
			
		});
		conn.close();
	}
	
	public static void main (String [] args ) {
		Dafiti.populaSkusNaCategoria();
		Digester.populaCategoriaSiteRecomendacoes(ConnHelper.get().reopen());
	}

}

