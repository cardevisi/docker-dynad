-- MySQL dump 10.13  Distrib 5.6.15, for Win32 (x86)
--
-- Host: localhost    Database: dynad_catho
-- ------------------------------------------------------
-- Server version	5.6.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_vars`
--

DROP TABLE IF EXISTS `active_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_vars` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `content` longtext,
  `customer_id` bigint(20) NOT NULL,
  `visitor_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC156CBC51A255F54` (`visitor_id`),
  KEY `FKC156CBC5E5CB6EBC` (`customer_id`),
  CONSTRAINT `FKC156CBC51A255F54` FOREIGN KEY (`visitor_id`) REFERENCES `visitor` (`id`),
  CONSTRAINT `FKC156CBC5E5CB6EBC` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_layer`
--

DROP TABLE IF EXISTS `ad_layer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_layer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `content` longblob,
  `index_order` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  `type` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKAE7D5E35FD0034F5` (`profile_id`),
  CONSTRAINT `FKAE7D5E35FD0034F5` FOREIGN KEY (`profile_id`) REFERENCES `ad_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_profile`
--

DROP TABLE IF EXISTS `ad_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `description` longtext,
  `height` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `responsible_id` bigint(20) NOT NULL,
  `status` varchar(1) NOT NULL,
  `updated_by_id` bigint(20) DEFAULT NULL,
  `width` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF4AD698D3A4C295B` (`updated_by_id`),
  KEY `FKF4AD698D97886AE0` (`responsible_id`),
  CONSTRAINT `FKF4AD698D3A4C295B` FOREIGN KEY (`updated_by_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FKF4AD698D97886AE0` FOREIGN KEY (`responsible_id`) REFERENCES `system_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `content` longblob NOT NULL,
  `date_created` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `responsible_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK58CEAF0DB754D14` (`campaign_id`),
  KEY `FK58CEAF097886AE0` (`responsible_id`),
  CONSTRAINT `FK58CEAF097886AE0` FOREIGN KEY (`responsible_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FK58CEAF0DB754D14` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audience`
--

DROP TABLE IF EXISTS `audience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audience` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `first_access` datetime NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `best_sellers`
--

DROP TABLE IF EXISTS `best_sellers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `best_sellers` (
  `id_sku` int(8) DEFAULT NULL,
  `versao` int(11) unsigned NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pos`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bkp_value`
--

DROP TABLE IF EXISTS `bkp_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bkp_value` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `version` bigint(20) NOT NULL,
  `index_order` bigint(20) NOT NULL,
  `label` longtext NOT NULL,
  `personalization_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadeia_visualizacao`
--

DROP TABLE IF EXISTS `cadeia_visualizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadeia_visualizacao` (
  `ID_SKU` int(11) DEFAULT NULL,
  `SKU` text CHARACTER SET utf8,
  `REC_ID_SKU` int(11) DEFAULT NULL,
  `REC_SKU` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `ad_profile_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `first_match` bit(1) DEFAULT NULL,
  `ios_enabled` bit(1) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `preview_dimension_height` bigint(20) DEFAULT NULL,
  `preview_dimension_width` bigint(20) DEFAULT NULL,
  `refresh_interval` bigint(20) NOT NULL,
  `responsible_id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `features` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF7A901109EE89A91` (`ad_profile_id`),
  KEY `FKF7A9011097886AE0` (`responsible_id`),
  CONSTRAINT `FKF7A9011097886AE0` FOREIGN KEY (`responsible_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FKF7A901109EE89A91` FOREIGN KEY (`ad_profile_id`) REFERENCES `ad_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `carousel`
--

DROP TABLE IF EXISTS `carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `carousel_personalization`
--

DROP TABLE IF EXISTS `carousel_personalization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel_personalization` (
  `personalization_id` bigint(20) NOT NULL,
  `carousel_id` bigint(20) NOT NULL,
  `offset` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `id` int(8) NOT NULL DEFAULT '0',
  `sku` varchar(45) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `imagem` varchar(512) DEFAULT NULL,
  `link` varchar(512) DEFAULT NULL,
  `preco_original` varchar(45) DEFAULT NULL,
  `preco_promocional` varchar(45) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `numero_parcelas` varchar(45) DEFAULT NULL,
  `valor_parcelas` varchar(45) DEFAULT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `recomendacoes` varchar(255) DEFAULT NULL,
  `categoria1` varchar(255) DEFAULT NULL,
  `categoria2` varchar(255) DEFAULT NULL,
  `categoria3` varchar(255) DEFAULT NULL,
  `categoria4` varchar(256) DEFAULT NULL,
  `categoria5` varchar(256) DEFAULT NULL,
  `id_sku` int(8) DEFAULT NULL,
  `versao` int(8) unsigned NOT NULL DEFAULT '1',
  `id_categoria` int(10) DEFAULT NULL,
  `ativo` char(1) NOT NULL DEFAULT '1',
  `codigo` varchar(60) DEFAULT NULL,
  `flag_recomendacao` int(1) DEFAULT NULL,
  `flag_oferta` int(1) DEFAULT NULL,
  `flag_novidade` int(1) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update` datetime DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_in` date DEFAULT NULL,
  `desconto` varchar(10) DEFAULT NULL,
  `selo` varchar(20) DEFAULT NULL,
  `imagem_sprite` varchar(512) DEFAULT NULL,
  `gratis` tinyint(1) DEFAULT NULL,
  `estado` varchar(32) DEFAULT NULL,
  `pais` varchar(80) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `recomendacoes_uol` varchar(255) DEFAULT NULL,
  `persisted_categoria1` varchar(512) DEFAULT NULL,
  `persisted_categoria2` varchar(512) DEFAULT NULL,
  `persisted_categoria3` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index_2` (`sku`),
  KEY `idx_cat1` (`categoria1`),
  KEY `idx_cat2` (`categoria2`),
  KEY `idx_cat3` (`categoria3`),
  KEY `idx_10` (`ativo`),
  KEY `idx_11` (`id_sku`),
  KEY `idx_12` (`id_categoria`),
  KEY `idx_13` (`versao`),
  KEY `idx_score` (`score`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `codigos_categorias`
--

DROP TABLE IF EXISTS `codigos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigos_categorias` (
  `categoria` varchar(512) DEFAULT NULL,
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `versao` int(11) unsigned NOT NULL DEFAULT '1',
  `categoria1` varchar(512) DEFAULT NULL,
  `categoria2` varchar(512) DEFAULT NULL,
  `categoria3` varchar(512) DEFAULT NULL,
  `categoria4` varchar(256) DEFAULT NULL,
  `categoria5` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42518 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `codigos_skus`
--

DROP TABLE IF EXISTS `codigos_skus`;
/*!50001 DROP VIEW IF EXISTS `codigos_skus`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `codigos_skus` (
  `sku` tinyint NOT NULL,
  `id` tinyint NOT NULL,
  `versao` tinyint NOT NULL,
  `id_categoria` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_network`
--

DROP TABLE IF EXISTS `customer_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_network` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `network_id` bigint(20) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `responsible_id` bigint(20) NOT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF3FBC2CD199C70FB` (`source_id`),
  KEY `FKF3FBC2CD713AD029` (`network_id`),
  KEY `FKF3FBC2CDE5CB6EBC` (`customer_id`),
  KEY `FKF3FBC2CD97886AE0` (`responsible_id`),
  CONSTRAINT `FKF3FBC2CD199C70FB` FOREIGN KEY (`source_id`) REFERENCES `match_source` (`id`),
  CONSTRAINT `FKF3FBC2CD713AD029` FOREIGN KEY (`network_id`) REFERENCES `network` (`id`),
  CONSTRAINT `FKF3FBC2CD97886AE0` FOREIGN KEY (`responsible_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FKF3FBC2CDE5CB6EBC` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_profile`
--

DROP TABLE IF EXISTS `customer_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_users`
--

DROP TABLE IF EXISTS `customer_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_users` (
  `customer_id` bigint(20) NOT NULL,
  `system_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`customer_id`,`system_user_id`),
  KEY `FK87309267E5CB6EBC` (`customer_id`),
  KEY `FK87309267C9EB93DB` (`system_user_id`),
  CONSTRAINT `FK87309267C9EB93DB` FOREIGN KEY (`system_user_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FK87309267E5CB6EBC` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dyn_event`
--

DROP TABLE IF EXISTS `dyn_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dyn_event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `info` longtext NOT NULL,
  `server_id` varchar(80) NOT NULL,
  `type` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_log`
--

DROP TABLE IF EXISTS `event_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `action` varchar(255) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `remote_addr` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1E4681FE5CB6EBC` (`customer_id`),
  CONSTRAINT `FK1E4681FE5CB6EBC` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook`
--

DROP TABLE IF EXISTS `facebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `about_me` varchar(255) DEFAULT NULL,
  `activities` varchar(255) DEFAULT NULL,
  `allowed_restrictions` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `birthday_date` datetime DEFAULT NULL,
  `books` varchar(255) DEFAULT NULL,
  `can_post` bit(1) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `current_location_city` varchar(255) DEFAULT NULL,
  `current_location_country` varchar(255) DEFAULT NULL,
  `current_location_id` varchar(255) DEFAULT NULL,
  `current_location_name` varchar(255) DEFAULT NULL,
  `current_location_state` varchar(255) DEFAULT NULL,
  `current_location_zip` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `education_school_id` varchar(255) DEFAULT NULL,
  `education_school_name` varchar(255) DEFAULT NULL,
  `education_school_type` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `favorite_athlete_id` varchar(255) DEFAULT NULL,
  `favorite_athlete_name` varchar(255) DEFAULT NULL,
  `favorite_team_id` varchar(255) DEFAULT NULL,
  `favorite_team_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `friend_count` int(11) DEFAULT NULL,
  `games` varchar(255) DEFAULT NULL,
  `hometown_location_city` varchar(255) DEFAULT NULL,
  `hometown_location_country` varchar(255) DEFAULT NULL,
  `hometown_location_id` varchar(255) DEFAULT NULL,
  `hometown_location_name` varchar(255) DEFAULT NULL,
  `hometown_location_state` varchar(255) DEFAULT NULL,
  `hometown_location_zip` varchar(255) DEFAULT NULL,
  `inspirational_people_id` varchar(255) DEFAULT NULL,
  `inspirational_people_name` varchar(255) DEFAULT NULL,
  `interests` varchar(255) DEFAULT NULL,
  `is_app_user` bit(1) DEFAULT NULL,
  `is_blocked` bit(1) DEFAULT NULL,
  `is_minor` bit(1) DEFAULT NULL,
  `language_id` varchar(255) DEFAULT NULL,
  `language_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `likes_count` int(11) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `meeting_sex_female` bit(1) DEFAULT NULL,
  `meeting_sex_male` bit(1) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `movies` varchar(255) DEFAULT NULL,
  `music` varchar(255) DEFAULT NULL,
  `mutual_friend_count` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `name_format` varchar(255) DEFAULT NULL,
  `notes_count` int(11) DEFAULT NULL,
  `online_presence` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `pic_big` varchar(255) DEFAULT NULL,
  `pic_big_with_logo` varchar(255) DEFAULT NULL,
  `pic_small` varchar(255) DEFAULT NULL,
  `pic_small_with_logo` varchar(255) DEFAULT NULL,
  `pic_square` varchar(255) DEFAULT NULL,
  `pic_square_with_logo` varchar(255) DEFAULT NULL,
  `pic_with_logo` varchar(255) DEFAULT NULL,
  `political` varchar(255) DEFAULT NULL,
  `profile_blurb` varchar(255) DEFAULT NULL,
  `profile_update_time` bigint(20) DEFAULT NULL,
  `profile_url` varchar(255) DEFAULT NULL,
  `proxied_email` varchar(255) DEFAULT NULL,
  `quotes` varchar(255) DEFAULT NULL,
  `relationship_status` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `significant_other_id` varchar(255) DEFAULT NULL,
  `sport_id` varchar(255) DEFAULT NULL,
  `sport_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `third_party_id` varchar(255) DEFAULT NULL,
  `timezone` int(11) DEFAULT NULL,
  `tv` varchar(255) DEFAULT NULL,
  `uid` bigint(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `verified` bit(1) DEFAULT NULL,
  `wall_count` int(11) DEFAULT NULL,
  `work_id` varchar(255) DEFAULT NULL,
  `work_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filter_expression`
--

DROP TABLE IF EXISTS `filter_expression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_expression` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `hql_expression` varchar(255) NOT NULL,
  `language_expression` varchar(255) NOT NULL,
  `var_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4639C1BF1DEF0F25` (`var_type_id`),
  CONSTRAINT `FK4639C1BF1DEF0F25` FOREIGN KEY (`var_type_id`) REFERENCES `var_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lixo`
--

DROP TABLE IF EXISTS `lixo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lixo` (
  `id` bigint(20) DEFAULT NULL,
  `texto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_agregator`
--

DROP TABLE IF EXISTS `match_agregator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_agregator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `hql_function` varchar(255) NOT NULL,
  `var_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK42F1670A1DEF0F25` (`var_type_id`),
  CONSTRAINT `FK42F1670A1DEF0F25` FOREIGN KEY (`var_type_id`) REFERENCES `var_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_custom_js`
--

DROP TABLE IF EXISTS `match_custom_js`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_custom_js` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(80) NOT NULL,
  `plugin` longtext,
  `source_id` bigint(20) NOT NULL,
  `status` varchar(1) NOT NULL,
  `url_pattern` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68BF86DD199C70FB` (`source_id`),
  CONSTRAINT `FK68BF86DD199C70FB` FOREIGN KEY (`source_id`) REFERENCES `match_source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_source`
--

DROP TABLE IF EXISTS `match_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `protocol` varchar(5) NOT NULL,
  `responsible_id` bigint(20) NOT NULL,
  `type` varchar(13) NOT NULL,
  `updated_by_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5883F3D53A4C295B` (`updated_by_id`),
  KEY `FK5883F3D597886AE0` (`responsible_id`),
  CONSTRAINT `FK5883F3D53A4C295B` FOREIGN KEY (`updated_by_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FK5883F3D597886AE0` FOREIGN KEY (`responsible_id`) REFERENCES `system_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_value`
--

DROP TABLE IF EXISTS `match_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `filter_expression_id` bigint(20) NOT NULL,
  `match_var_id` bigint(20) NOT NULL,
  `personalization_ids` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `novo` int(1) unsigned DEFAULT NULL,
  `filter_priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK769BA677DB754D14` (`campaign_id`),
  KEY `FK769BA6774EF88A4F` (`filter_expression_id`),
  KEY `FK769BA677BDC01A93` (`match_var_id`),
  KEY `idx_sku` (`value`),
  KEY `idx_matval_pers1` (`personalization_ids`),
  CONSTRAINT `FK769BA6774EF88A4F` FOREIGN KEY (`filter_expression_id`) REFERENCES `filter_expression` (`id`),
  CONSTRAINT `FK769BA677BDC01A93` FOREIGN KEY (`match_var_id`) REFERENCES `match_var` (`id`),
  CONSTRAINT `FK769BA677DB754D14` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_var`
--

DROP TABLE IF EXISTS `match_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_var` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `history` bit(1) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_system` varchar(80) DEFAULT NULL,
  `ranking` bit(1) DEFAULT NULL,
  `ref_id` bigint(20) DEFAULT NULL,
  `source_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK249E65AD199C70FB` (`source_id`),
  KEY `FK249E65AD59A7B83D` (`type_id`),
  CONSTRAINT `FK249E65AD199C70FB` FOREIGN KEY (`source_id`) REFERENCES `match_source` (`id`),
  CONSTRAINT `FK249E65AD59A7B83D` FOREIGN KEY (`type_id`) REFERENCES `var_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `navigation_var`
--

DROP TABLE IF EXISTS `navigation_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation_var` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `label` varchar(25) NOT NULL,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `net_facebook`
--

DROP TABLE IF EXISTS `net_facebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `net_facebook` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `facebook_id` bigint(20) NOT NULL,
  `social_network_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE0DDA328329A9B7D` (`social_network_id`),
  KEY `FKE0DDA328323924AB` (`facebook_id`),
  CONSTRAINT `FKE0DDA328323924AB` FOREIGN KEY (`facebook_id`) REFERENCES `facebook` (`id`),
  CONSTRAINT `FKE0DDA328329A9B7D` FOREIGN KEY (`social_network_id`) REFERENCES `customer_network` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `network`
--

DROP TABLE IF EXISTS `network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `domain_class` varchar(255) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `network_filter`
--

DROP TABLE IF EXISTS `network_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `domain_property` varchar(255) NOT NULL,
  `network_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8CB43EA9713AD029` (`network_id`),
  KEY `FK8CB43EA959A7B83D` (`type_id`),
  CONSTRAINT `FK8CB43EA959A7B83D` FOREIGN KEY (`type_id`) REFERENCES `var_type` (`id`),
  CONSTRAINT `FK8CB43EA9713AD029` FOREIGN KEY (`network_id`) REFERENCES `network` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `params_clicktag`
--

DROP TABLE IF EXISTS `params_clicktag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `params_clicktag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_portal` varchar(255) DEFAULT NULL,
  `id_pagina` varchar(255) DEFAULT NULL,
  `formato_criativo` varchar(255) DEFAULT NULL,
  `descricao_campanha` varchar(255) DEFAULT NULL,
  `padrao` int(1) unsigned NOT NULL,
  `ultima_atualizacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `formato_url` varchar(255) NOT NULL,
  `descricao_criativo` varchar(255) NOT NULL,
  `escopo` varchar(45) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `aplicar` int(1) DEFAULT NULL,
  `padrao_geral` int(1) DEFAULT NULL,
  `id_portal_dfa` varchar(45) DEFAULT NULL,
  `versao` int(8) DEFAULT NULL,
  `utilizacao` int(1) DEFAULT NULL,
  `bkp_formato_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personalization`
--

DROP TABLE IF EXISTS `personalization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `return_by_default` bit(1) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `novo` int(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK27751624DB754D14` (`campaign_id`),
  CONSTRAINT `FK27751624DB754D14` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personalization_bkp`
--

DROP TABLE IF EXISTS `personalization_bkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalization_bkp` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `version` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `return_by_default` bit(1) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `novo` int(1) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile_var`
--

DROP TABLE IF EXISTS `profile_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_var` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `agregator_id` bigint(20) NOT NULL,
  `content` varchar(255) NOT NULL,
  `index_order` int(11) NOT NULL,
  `variable_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA9C8391EC904764` (`variable_id`),
  KEY `FKA9C83915DE1CA79` (`agregator_id`),
  CONSTRAINT `FKA9C83915DE1CA79` FOREIGN KEY (`agregator_id`) REFERENCES `match_agregator` (`id`),
  CONSTRAINT `FKA9C8391EC904764` FOREIGN KEY (`variable_id`) REFERENCES `match_var` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pushed_var`
--

DROP TABLE IF EXISTS `pushed_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushed_var` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `match_var_id` bigint(20) NOT NULL,
  `value` longtext,
  `visitor_instance_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7D1F0F21EF74177C` (`visitor_instance_id`),
  KEY `FK7D1F0F21BDC01A93` (`match_var_id`),
  CONSTRAINT `FK7D1F0F21BDC01A93` FOREIGN KEY (`match_var_id`) REFERENCES `match_var` (`id`),
  CONSTRAINT `FK7D1F0F21EF74177C` FOREIGN KEY (`visitor_instance_id`) REFERENCES `visitor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ranking`
--

DROP TABLE IF EXISTS `ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking` (
  `sku` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ranking_by_day`
--

DROP TABLE IF EXISTS `ranking_by_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_by_day` (
  `sku` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  KEY `idx_date` (`date`),
  KEY `idx_source` (`source`),
  KEY `idx_sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_map`
--

DROP TABLE IF EXISTS `request_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `config_attribute` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `summary_by_clicktag`
--

DROP TABLE IF EXISTS `summary_by_clicktag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `summary_by_clicktag` (
  `date` date NOT NULL DEFAULT '0000-00-00',
  `impressions` int(11) DEFAULT NULL,
  `click_through` int(11) DEFAULT NULL,
  `source` varchar(255) NOT NULL DEFAULT '',
  `params_clicktag_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`date`,`source`,`params_clicktag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `summary_by_day`
--

DROP TABLE IF EXISTS `summary_by_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `summary_by_day` (
  `date` date NOT NULL DEFAULT '0000-00-00',
  `impressions` int(11) DEFAULT NULL,
  `click_through` int(11) DEFAULT NULL,
  `source` varchar(255) NOT NULL DEFAULT '',
  `placement` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`date`,`source`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_profile`
--

DROP TABLE IF EXISTS `system_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `authority` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority` (`authority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_profile_people`
--

DROP TABLE IF EXISTS `system_profile_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_profile_people` (
  `system_profile_id` bigint(20) NOT NULL,
  `system_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`system_profile_id`,`system_user_id`),
  KEY `FKA2139DF5EF4A1F79` (`system_profile_id`),
  KEY `FKA2139DF5C9EB93DB` (`system_user_id`),
  CONSTRAINT `FKA2139DF5C9EB93DB` FOREIGN KEY (`system_user_id`) REFERENCES `system_user` (`id`),
  CONSTRAINT `FKA2139DF5EF4A1F79` FOREIGN KEY (`system_profile_id`) REFERENCES `system_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_user`
--

DROP TABLE IF EXISTS `system_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_show` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `user_real_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp`
--

DROP TABLE IF EXISTS `temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp` (
  `id` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `top_sellers_categoria`
--

DROP TABLE IF EXISTS `top_sellers_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `top_sellers_categoria` (
  `id_categoria` int(11) DEFAULT NULL,
  `id_sku` int(11) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `versao` int(11) DEFAULT NULL,
  `pos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `top_sellers_genero`
--

DROP TABLE IF EXISTS `top_sellers_genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `top_sellers_genero` (
  `id_sku` int(8) DEFAULT NULL,
  `genero` varchar(20) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `versao` int(11) DEFAULT NULL,
  `pos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `value`
--

DROP TABLE IF EXISTS `value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `index_order` bigint(20) NOT NULL,
  `label` varchar(256) CHARACTER SET latin1 NOT NULL,
  `personalization_id` bigint(20) NOT NULL,
  `type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `value` varchar(1024) CHARACTER SET latin1 DEFAULT NULL,
  `novo` int(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6AC91715ACE00A0` (`personalization_id`),
  KEY `Index_3` (`label`,`value`(767)) USING HASH,
  CONSTRAINT `FK6AC91715ACE00A0` FOREIGN KEY (`personalization_id`) REFERENCES `personalization` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `value_bkp`
--

DROP TABLE IF EXISTS `value_bkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `value_bkp` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `version` bigint(20) NOT NULL,
  `index_order` bigint(20) NOT NULL,
  `label` varchar(256) NOT NULL,
  `personalization_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `novo` int(1) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `var_type`
--

DROP TABLE IF EXISTS `var_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `var_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitor`
--

DROP TABLE IF EXISTS `visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `cookie_version` varchar(255) NOT NULL,
  `data_motion_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `dyn_ad_id` varchar(255) NOT NULL,
  `facebook_id` bigint(20) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `registration_date` datetime NOT NULL,
  `twitter_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1BD2346E323924AB` (`facebook_id`),
  CONSTRAINT `FK1BD2346E323924AB` FOREIGN KEY (`facebook_id`) REFERENCES `facebook` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'dynad_catho'
--
/*!50003 DROP FUNCTION IF EXISTS `autoNormalizaMoeda` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `autoNormalizaMoeda`(s varchar(16)) RETURNS decimal(30,2)
    NO SQL
    DETERMINISTIC
BEGIN

DECLARE saida decimal(30,2);
DECLARE tam INT(2);
DECLARE t1 INT(2);

set t1 = 0;

if length(substring_index(s, ',', -1)) = 2 or length(substring_index(s, '.', -1)) = 2 then set t1 = 2; end if;

set s = NumericOnly(s);

select length(s) into tam from dual;

if t1 = 2 then select concat(substring(s, 1, length(s)-2), '.', substring(s, length(s)-1)) into s from dual; end if;

if tam <= 2 then return cast(s as decimal(30,0)); end if;

if t1 = 2 then return CAST(s AS DECIMAL(30, 2)); else return CAST(s AS DECIMAL(30, 0));  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_distance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_distance`( lat1  numeric (9,6),
 lon1  numeric (9,6),
 lat2  numeric (9,6),
 lon2  numeric (9,6)
) RETURNS decimal(10,5)
    READS SQL DATA
BEGIN
  DECLARE  x  decimal (20,10);
  DECLARE  pi  decimal (21,20);
  SET  pi = 3.14159265358979323846;
  SET  x = sin( lat1 * pi/180 ) * sin( lat2 * pi/180  ) + cos(
 lat1 *pi/180 ) * cos( lat2 * pi/180 ) * cos(  abs( (lon2 * pi/180) -
 (lon1 *pi/180) ) );
  SET  x = acos( x );
  RETURN  ( 1.852 * 60.0 * ((x/pi)*180) ) / 1.609344;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DEBUG` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `DEBUG`(mes varchar(512))
BEGIN
    SELECT mes  AS "Statement";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `popula_supernova` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `popula_supernova`()
BEGIN

   DECLARE done INT DEFAULT FALSE;
   DECLARE v_id int(8);
   DECLARE v_sku varchar(45);
   DECLARE v_categoria1 varchar(256);
   DECLARE v_categoria2 varchar(256);
   DECLARE v_categoria3 varchar(256);
   DECLARE cnt int(8);
   DECLARE v_id_sku int;
   DECLARE v_id_categoria int;
   DECLARE v_key int;
   DECLARE v_versao bigint(19);
	
	SET v_versao = UNIX_TIMESTAMP();
	
	UPDATE catalogo SET 
		categoria1 = IFNULL(categoria1, 'empty'), 
		categoria2 = IFNULL(categoria2, 'empty'), 
		categoria3 = IFNULL(categoria3, 'empty') 
	WHERE
		categoria1 is null or categoria2 is null or categoria3 is null;
	
	
	INSERT INTO codigos_categorias (categoria, categoria1, categoria2, categoria3, versao)
	SELECT DISTINCT 
		concat(concat(concat(concat(IFNULL(categoria1,'empty'), '/'),IFNULL(categoria2,'empty')), '/'),IFNULL(categoria3,'empty')), 
		categoria1, 
		categoria2, 
		categoria3,
		v_versao
	FROM catalogo 
	WHERE 
		id_categoria is null and 
		(IFNULL(categoria1,'empty'), IFNULL(categoria2,'empty'), IFNULL(categoria3,'empty')) 
		not in (select distinct categoria1, categoria2, categoria3 from codigos_categorias);
	
	
	update 
		catalogo a 
	set 
		a.persisted_categoria1 =ifnull(a.categoria1, 'empty'), 
		a.persisted_categoria2 =ifnull(a.categoria2, 'empty'), 
		a.persisted_categoria3 =ifnull(a.categoria3, 'empty'), 
		a.id_categoria = (select b.id from codigos_categorias b where b.categoria1 = ifnull(a.categoria1, 'empty') 
			and b.categoria2 = ifnull(a.categoria2, 'empty')
			and b.categoria3 = ifnull(a.categoria3, 'empty') )
	where 
		a.id_categoria is null;
	
	
	INSERT INTO codigos_categorias (categoria, categoria1, categoria2, categoria3, versao)
	SELECT DISTINCT 
		concat(concat(concat(concat(IFNULL(categoria1,'empty'), '/'),IFNULL(categoria2,'empty')), '/'),IFNULL(categoria3,'empty')), 
		categoria1, 
		categoria2, 
		categoria3,
		v_versao
	FROM catalogo 
	WHERE 
		id_categoria is not null 
		and ( persisted_categoria1 <> IFNULL(categoria1,'empty') or persisted_categoria2 <> IFNULL(categoria2,'empty') or persisted_categoria3 <> IFNULL(categoria3,'empty') )
		and (IFNULL(categoria1,'empty'), IFNULL(categoria2,'empty'), IFNULL(categoria3,'empty')) 
		not in (select distinct categoria1, categoria2, categoria3 from codigos_categorias);
	
	
	update 
		catalogo a 
	set 
		a.persisted_categoria1 =ifnull(a.categoria1, 'empty'), 
		a.persisted_categoria2 =ifnull(a.categoria2, 'empty'), 
		a.persisted_categoria3 =ifnull(a.categoria3, 'empty'), 
		a.id_categoria = (select b.id from codigos_categorias b where b.categoria1 = ifnull(a.categoria1, 'empty') 
			and b.categoria2 = ifnull(a.categoria2, 'empty')
			and b.categoria3 = ifnull(a.categoria3, 'empty') )
	where 
		a.id_categoria is not null 
		and ( a.persisted_categoria1 <> IFNULL(a.categoria1,'empty') 
				or a.persisted_categoria2 <> IFNULL(a.categoria2,'empty') 
				or a.persisted_categoria3 <> IFNULL(a.categoria3,'empty') );	
	
	
   UPDATE catalogo a SET a.versao = unix_timestamp(), a.id_sku = a.id WHERE a.id_sku <> a.id or a.id_sku is null;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `codigos_skus`
--

/*!50001 DROP TABLE IF EXISTS `codigos_skus`*/;
/*!50001 DROP VIEW IF EXISTS `codigos_skus`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `codigos_skus` AS select `catalogo`.`sku` AS `sku`,`catalogo`.`id` AS `id`,`catalogo`.`versao` AS `versao`,`catalogo`.`id_categoria` AS `id_categoria` from `catalogo` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-14 16:56:22
